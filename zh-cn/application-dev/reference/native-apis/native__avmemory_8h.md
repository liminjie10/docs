# native_avmemory.h


## 概述

声明了AVMemory的函数接口。

**起始版本：**
9
**Version:**
1.0
**相关模块:**

[Core](_core.md)


## 汇总


### 类型定义

  | 名称 | 描述 | 
| -------- | -------- |
| **OH_AVMemory** | typedef struct OH_AVMemory | 


### 函数

  | 名称 | 描述 | 
| -------- | -------- |
| [OH_AVMemory_GetAddr](_core.md#oh_avmemory_getaddr) (struct OH_AVMemory \*mem) | uint8_t \*<br/>获取入参的内存虚拟地址。  | 
| [OH_AVMemory_GetSize](_core.md#oh_avmemory_getsize) (struct OH_AVMemory \*mem) | int32_t<br/>获取入参的内存长度。  | 
