# OH_AVCodecBufferAttr


## 概述

定义OH_AVCodec的Buffer描述信息。

@syscap SystemCapability.Multimedia.Media.CodecBase

**起始版本：**
9

**相关模块:**

[CodecBase](_codec_base.md)


## 汇总


### 成员变量

  | 名称 | 描述 | 
| -------- | -------- |
| [pts](_codec_base.md#pts) | int64_t | 
| [size](_codec_base.md#size) | int32_t | 
| [offset](_codec_base.md#offset) | int32_t | 
| [flags](_codec_base.md#flags) | uint32_t | 
