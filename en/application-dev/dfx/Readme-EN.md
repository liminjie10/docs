# DFX

- Application Event Logging
  - [Development of Application Event Logging](hiappevent-guidelines.md)
- Distributed Call Chain Tracing
  - [Development of Distributed Call Chain Tracing](hitracechain-guidelines.md)
- HiLog
  - [HiLog Development](hilog-guidelines.md)
- Performance Tracing
  - [Development of Performance Tracing](hitracemeter-guidelines.md)
- Error Management
  - [Development of Error Manager](errormanager-guidelines.md)
  - [Development of Application Recovery](apprecovery-guidelines.md)
