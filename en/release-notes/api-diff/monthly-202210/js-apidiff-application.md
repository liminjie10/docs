| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.contact<br>Class name: Contact|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: INVALID_CONTACT_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: id|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: key|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: contactAttributes|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: emails|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: events|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: groups|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: imAddresses|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: phoneNumbers|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: portrait|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: postalAddresses|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: relations|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: sipAddresses|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: websites|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: name|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: nickName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: note|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Contact<br>Method or attribute name: organization|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ContactAttributes|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ContactAttributes<br>Method or attribute name: attributes|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_CONTACT_EVENT|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_EMAIL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_GROUP_MEMBERSHIP|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_IM|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_NAME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_NICKNAME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_NOTE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_ORGANIZATION|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_PHONE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_PORTRAIT|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_POSTAL_ADDRESS|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_RELATION|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_SIP_ADDRESS|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Attribute<br>Method or attribute name: ATTR_WEBSITE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: EMAIL_HOME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: EMAIL_WORK|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: EMAIL_OTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: email|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: displayName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Email<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: EVENT_ANNIVERSARY|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: EVENT_OTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: EVENT_BIRTHDAY|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: eventDate|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Event<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Group|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Group<br>Method or attribute name: groupId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Group<br>Method or attribute name: title|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Holder|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Holder<br>Method or attribute name: bundleName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Holder<br>Method or attribute name: displayName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Holder<br>Method or attribute name: holderId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_AIM|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_MSN|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_YAHOO|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_SKYPE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_QQ|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_ICQ|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: IM_JABBER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: imAddress|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: ImAddress<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: familyName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: familyNamePhonetic|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: fullName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: givenName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: givenNamePhonetic|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: middleName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: middleNamePhonetic|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: namePrefix|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Name<br>Method or attribute name: nameSuffix|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: NickName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: NickName<br>Method or attribute name: nickName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Note|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Note<br>Method or attribute name: noteContent|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Organization|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Organization<br>Method or attribute name: name|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Organization<br>Method or attribute name: title|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_HOME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_MOBILE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_WORK|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_FAX_WORK|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_FAX_HOME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_PAGER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_OTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_CALLBACK|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_CAR|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_COMPANY_MAIN|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_ISDN|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_MAIN|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_OTHER_FAX|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_RADIO|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_TELEX|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_TTY_TDD|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_WORK_MOBILE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_WORK_PAGER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_ASSISTANT|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: NUM_MMS|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: phoneNumber|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PhoneNumber<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Portrait|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Portrait<br>Method or attribute name: uri|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: ADDR_HOME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: ADDR_WORK|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: ADDR_OTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: city|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: country|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: neighborhood|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: pobox|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: postalAddress|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: postcode|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: region|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: street|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: PostalAddress<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_ASSISTANT|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_BROTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_CHILD|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_DOMESTIC_PARTNER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_FATHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_FRIEND|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_MANAGER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_MOTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_PARENT|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_PARTNER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_REFERRED_BY|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_RELATIVE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_SISTER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: RELATION_SPOUSE|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: relationName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Relation<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: CUSTOM_LABEL|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: SIP_HOME|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: SIP_WORK|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: SIP_OTHER|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: INVALID_LABEL_ID|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: labelName|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: sipAddress|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: SipAddress<br>Method or attribute name: labelId|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Website|@ohos.contact.d.ts|
|Added||Module name: ohos.contact<br>Class name: Website<br>Method or attribute name: website|@ohos.contact.d.ts|
|Added||Module name: ohos.telephony.call<br>Class name:  AudioDevice<br>Method or attribute name: DEVICE_MIC|@ohos.telephony.call.d.ts|
