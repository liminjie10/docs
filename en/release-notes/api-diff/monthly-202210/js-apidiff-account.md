| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: createAccount|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: createAccount|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: createAccount|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: createAccountImplicitly|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: createAccountImplicitly|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: removeAccount|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: removeAccount|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setAppAccess|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setAppAccess|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: checkDataSyncEnabled|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: checkDataSyncEnabled|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setCredential|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setCredential|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setDataSyncEnabled|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setDataSyncEnabled|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setCustomData|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setCustomData|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: getAllAccounts<br>Function name: getAllAccounts(callback: AsyncCallback<Array<AppAccountInfo>>): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: getAllAccounts<br>Function name: getAllAccounts(): Promise<Array<AppAccountInfo>>;|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAccountsByOwner|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAccountsByOwner|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getCredential|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getCredential|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getCustomData|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getCustomData|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getCustomDataSync|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: on_accountChange|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: off_accountChange|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: auth|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: auth|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAuthToken|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAuthToken|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setAuthToken|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setAuthToken|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: deleteAuthToken|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: deleteAuthToken|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setAuthTokenVisibility|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: setAuthTokenVisibility|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: checkAuthTokenVisibility|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: checkAuthTokenVisibility|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAllAuthTokens|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAllAuthTokens|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAuthList|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAuthList|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAuthCallback|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAuthCallback|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: queryAuthenticatorInfo|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: queryAuthenticatorInfo|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: deleteCredential|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: deleteCredential|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: selectAccountsByOptions<br>Function name: selectAccountsByOptions(options: SelectAccountsOptions, callback: AsyncCallback<Array<AppAccountInfo>>): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: verifyCredential<br>Function name: verifyCredential(name: string, owner: string, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: verifyCredential<br>Function name: verifyCredential(name: string, owner: string, options: VerifyCredentialOptions, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: setAuthenticatorProperties<br>Function name: setAuthenticatorProperties(owner: string, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: setAuthenticatorProperties<br>Function name: setAuthenticatorProperties(owner: string, options: SetPropertiesOptions, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthTokenInfo|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthTokenInfo<br>Method or attribute name: authType|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthTokenInfo<br>Method or attribute name: token|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthTokenInfo<br>Method or attribute name: account|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthResult|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthResult<br>Method or attribute name: account|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthResult<br>Method or attribute name: tokenInfo|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: CreateAccountOptions|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: CreateAccountOptions<br>Method or attribute name: customData|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: CreateAccountImplicitlyOptions|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: CreateAccountImplicitlyOptions<br>Method or attribute name: requiredLabels|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: CreateAccountImplicitlyOptions<br>Method or attribute name: authType|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: CreateAccountImplicitlyOptions<br>Method or attribute name: parameters|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: Constants<br>Method or attribute name: ACTION_CREATE_ACCOUNT_IMPLICITLY|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: Constants<br>Method or attribute name: ACTION_AUTH|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: Constants<br>Method or attribute name: ACTION_VERIFY_CREDENTIAL|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: Constants<br>Method or attribute name: ACTION_SET_AUTHENTICATOR_PROPERTIES|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthCallback|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthCallback<br>Method or attribute name: onResult|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthCallback<br>Method or attribute name: onRequestRedirected|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: AuthCallback<br>Method or attribute name: onRequestContinued|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: Authenticator<br>Method or attribute name: createAccountImplicitly|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.appAccount<br>Class name: Authenticator<br>Method or attribute name: auth|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: verifyCredential<br>Function name: verifyCredential(name: string, options: VerifyCredentialOptions, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: setProperties<br>Function name: setProperties(options: SetPropertiesOptions, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: checkAccountLabels<br>Function name: checkAccountLabels(name: string, labels: Array<string>, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Method or attribute name: isAccountRemovable<br>Function name: isAccountRemovable(name: string, callback: AuthCallback): void;|@ohos.account.appAccount.d.ts|
|Added||Module name: ohos.account.distributedAccount<br>Class name: DistributedAccountAbility<br>Method or attribute name: getOsAccountDistributedInfo|@ohos.account.distributedAccount.d.ts|
|Added||Module name: ohos.account.distributedAccount<br>Class name: DistributedAccountAbility<br>Method or attribute name: getOsAccountDistributedInfo|@ohos.account.distributedAccount.d.ts|
|Added||Module name: ohos.account.distributedAccount<br>Class name: DistributedAccountAbility<br>Method or attribute name: setOsAccountDistributedInfo|@ohos.account.distributedAccount.d.ts|
|Added||Module name: ohos.account.distributedAccount<br>Class name: DistributedAccountAbility<br>Method or attribute name: setOsAccountDistributedInfo|@ohos.account.distributedAccount.d.ts|
|Added||Module name: ohos.account.distributedAccount<br>Class name: DistributedInfo<br>Method or attribute name: nickname|@ohos.account.distributedAccount.d.ts|
|Added||Module name: ohos.account.distributedAccount<br>Class name: DistributedInfo<br>Method or attribute name: avatar|@ohos.account.distributedAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkMultiOsAccountEnabled|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkMultiOsAccountEnabled|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountActivated|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountActivated|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkConstraintEnabled|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkConstraintEnabled|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountTestable|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountTestable|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountVerified|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountVerified|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: checkOsAccountVerified|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getOsAccountCount|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getOsAccountCount|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdFromProcess|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdFromProcess|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdFromUid|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdFromUid|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdFromDomain|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdFromDomain|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getOsAccountConstraints|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getOsAccountConstraints|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getActivatedOsAccountIds|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getActivatedOsAccountIds|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getCurrentOsAccount|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getCurrentOsAccount|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getOsAccountType|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: getOsAccountType|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryDistributedVirtualDeviceId|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryDistributedVirtualDeviceId|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdBySerialNumber|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: queryOsAccountLocalIdBySerialNumber|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: querySerialNumberByOsAccountLocalId|@ohos.account.osAccount.d.ts|
|Added||Module name: ohos.account.osAccount<br>Class name: AccountManager<br>Method or attribute name: querySerialNumberByOsAccountLocalId|@ohos.account.osAccount.d.ts|
|Added||Method or attribute name: cancelAuth<br>Function name: cancelAuth(contextID: Uint8Array): void;|@ohos.account.osAccount.d.ts|
|Added||Method or attribute name: registerInputer<br>Function name: registerInputer(inputer: IInputer): void;|@ohos.account.osAccount.d.ts|
|Added||Method or attribute name: cancel<br>Function name: cancel(challenge: Uint8Array): void;|@ohos.account.osAccount.d.ts|
|Deleted|Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: getAssociatedDataSync||@ohos.account.appAccount.d.ts|
|Deleted|Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: deleteAccountCredential||@ohos.account.appAccount.d.ts|
|Deleted|Module name: ohos.account.appAccount<br>Class name: AppAccountManager<br>Method or attribute name: deleteAccountCredential||@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: addAccount<br>Deprecated version: N/A|Method or attribute name: addAccount<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: addAccount<br>Deprecated version: N/A|Method or attribute name: addAccount<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: addAccount<br>Deprecated version: N/A|Method or attribute name: addAccount<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: addAccountImplicitly<br>Deprecated version: N/A|Method or attribute name: addAccountImplicitly<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: deleteAccount<br>Deprecated version: N/A|Method or attribute name: deleteAccount<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: deleteAccount<br>Deprecated version: N/A|Method or attribute name: deleteAccount<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: disableAppAccess<br>Deprecated version: N/A|Method or attribute name: disableAppAccess<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: disableAppAccess<br>Deprecated version: N/A|Method or attribute name: disableAppAccess<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: enableAppAccess<br>Deprecated version: N/A|Method or attribute name: enableAppAccess<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: enableAppAccess<br>Deprecated version: N/A|Method or attribute name: enableAppAccess<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: checkAppAccountSyncEnable<br>Deprecated version: N/A|Method or attribute name: checkAppAccountSyncEnable<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: checkAppAccountSyncEnable<br>Deprecated version: N/A|Method or attribute name: checkAppAccountSyncEnable<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAccountCredential<br>Deprecated version: N/A|Method or attribute name: setAccountCredential<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAccountCredential<br>Deprecated version: N/A|Method or attribute name: setAccountCredential<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAccountExtraInfo<br>Deprecated version: N/A|Method or attribute name: setAccountExtraInfo<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAccountExtraInfo<br>Deprecated version: N/A|Method or attribute name: setAccountExtraInfo<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAppAccountSyncEnable<br>Deprecated version: N/A|Method or attribute name: setAppAccountSyncEnable<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAppAccountSyncEnable<br>Deprecated version: N/A|Method or attribute name: setAppAccountSyncEnable<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAssociatedData<br>Deprecated version: N/A|Method or attribute name: setAssociatedData<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setAssociatedData<br>Deprecated version: N/A|Method or attribute name: setAssociatedData<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAllAccessibleAccounts<br>Deprecated version: N/A|Method or attribute name: getAllAccessibleAccounts<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAllAccessibleAccounts<br>Deprecated version: N/A|Method or attribute name: getAllAccessibleAccounts<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAllAccounts<br>Deprecated version: N/A|Method or attribute name: getAllAccounts<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAllAccounts<br>Deprecated version: N/A|Method or attribute name: getAllAccounts<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAccountCredential<br>Deprecated version: N/A|Method or attribute name: getAccountCredential<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAccountCredential<br>Deprecated version: N/A|Method or attribute name: getAccountCredential<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAccountExtraInfo<br>Deprecated version: N/A|Method or attribute name: getAccountExtraInfo<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAccountExtraInfo<br>Deprecated version: N/A|Method or attribute name: getAccountExtraInfo<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAssociatedData<br>Deprecated version: N/A|Method or attribute name: getAssociatedData<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAssociatedData<br>Deprecated version: N/A|Method or attribute name: getAssociatedData<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: on_change<br>Deprecated version: N/A|Method or attribute name: on_change<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: off_change<br>Deprecated version: N/A|Method or attribute name: off_change<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: authenticate<br>Deprecated version: N/A|Method or attribute name: authenticate<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOAuthToken<br>Deprecated version: N/A|Method or attribute name: getOAuthToken<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOAuthToken<br>Deprecated version: N/A|Method or attribute name: getOAuthToken<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setOAuthToken<br>Deprecated version: N/A|Method or attribute name: setOAuthToken<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setOAuthToken<br>Deprecated version: N/A|Method or attribute name: setOAuthToken<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: deleteOAuthToken<br>Deprecated version: N/A|Method or attribute name: deleteOAuthToken<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: deleteOAuthToken<br>Deprecated version: N/A|Method or attribute name: deleteOAuthToken<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setOAuthTokenVisibility<br>Deprecated version: N/A|Method or attribute name: setOAuthTokenVisibility<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: setOAuthTokenVisibility<br>Deprecated version: N/A|Method or attribute name: setOAuthTokenVisibility<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: checkOAuthTokenVisibility<br>Deprecated version: N/A|Method or attribute name: checkOAuthTokenVisibility<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: checkOAuthTokenVisibility<br>Deprecated version: N/A|Method or attribute name: checkOAuthTokenVisibility<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAllOAuthTokens<br>Deprecated version: N/A|Method or attribute name: getAllOAuthTokens<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAllOAuthTokens<br>Deprecated version: N/A|Method or attribute name: getAllOAuthTokens<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOAuthList<br>Deprecated version: N/A|Method or attribute name: getOAuthList<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOAuthList<br>Deprecated version: N/A|Method or attribute name: getOAuthList<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAuthenticatorCallback<br>Deprecated version: N/A|Method or attribute name: getAuthenticatorCallback<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAuthenticatorCallback<br>Deprecated version: N/A|Method or attribute name: getAuthenticatorCallback<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAuthenticatorInfo<br>Deprecated version: N/A|Method or attribute name: getAuthenticatorInfo<br>Deprecated version: 9<br>New API: appAccount.AppAccountManager|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: getAuthenticatorInfo<br>Deprecated version: N/A|Method or attribute name: getAuthenticatorInfo<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Class name: OAuthTokenInfo<br>Deprecated version: N/A|Class name: OAuthTokenInfo<br>Deprecated version: 9<br>New API: appAccount.AuthTokenInfo     |@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: ACTION_ADD_ACCOUNT_IMPLICITLY<br>Deprecated version: N/A|Method or attribute name: ACTION_ADD_ACCOUNT_IMPLICITLY<br>Deprecated version: 9<br>New API: appAccount.Constants|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: ACTION_AUTHENTICATE<br>Deprecated version: N/A|Method or attribute name: ACTION_AUTHENTICATE<br>Deprecated version: 9<br>New API: appAccount.Constants|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Class name: ResultCode<br>Deprecated version: N/A|Class name: ResultCode<br>Deprecated version: 9|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Class name: AuthenticatorCallback<br>Deprecated version: N/A|Class name: AuthenticatorCallback<br>Deprecated version: 9<br>New API: AppAccount.AuthCallback     |@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: addAccountImplicitly<br>Deprecated version: N/A|Method or attribute name: addAccountImplicitly<br>Deprecated version: 9<br>New API: appAccount.Authenticator|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: authenticate<br>Deprecated version: N/A|Method or attribute name: authenticate<br>Deprecated version: 9<br>New API: appAccount.Authenticator|@ohos.account.appAccount.d.ts|
|Deprecated version changed|Method or attribute name: queryOsAccountDistributedInfo<br>Deprecated version: N/A|Method or attribute name: queryOsAccountDistributedInfo<br>Deprecated version: 9<br>New API: distributedAccount.DistributedAccountAbility|@ohos.account.distributedAccount.d.ts|
|Deprecated version changed|Method or attribute name: queryOsAccountDistributedInfo<br>Deprecated version: N/A|Method or attribute name: queryOsAccountDistributedInfo<br>Deprecated version: 9|@ohos.account.distributedAccount.d.ts|
|Deprecated version changed|Method or attribute name: updateOsAccountDistributedInfo<br>Deprecated version: N/A|Method or attribute name: updateOsAccountDistributedInfo<br>Deprecated version: 9<br>New API: distributedAccount.DistributedAccountAbility|@ohos.account.distributedAccount.d.ts|
|Deprecated version changed|Method or attribute name: updateOsAccountDistributedInfo<br>Deprecated version: N/A|Method or attribute name: updateOsAccountDistributedInfo<br>Deprecated version: 9|@ohos.account.distributedAccount.d.ts|
|Deprecated version changed|Method or attribute name: isMultiOsAccountEnable<br>Deprecated version: N/A|Method or attribute name: isMultiOsAccountEnable<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isMultiOsAccountEnable<br>Deprecated version: N/A|Method or attribute name: isMultiOsAccountEnable<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountActived<br>Deprecated version: N/A|Method or attribute name: isOsAccountActived<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountActived<br>Deprecated version: N/A|Method or attribute name: isOsAccountActived<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountConstraintEnable<br>Deprecated version: N/A|Method or attribute name: isOsAccountConstraintEnable<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountConstraintEnable<br>Deprecated version: N/A|Method or attribute name: isOsAccountConstraintEnable<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isTestOsAccount<br>Deprecated version: N/A|Method or attribute name: isTestOsAccount<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isTestOsAccount<br>Deprecated version: N/A|Method or attribute name: isTestOsAccount<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountVerified<br>Deprecated version: N/A|Method or attribute name: isOsAccountVerified<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountVerified<br>Deprecated version: N/A|Method or attribute name: isOsAccountVerified<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: isOsAccountVerified<br>Deprecated version: N/A|Method or attribute name: isOsAccountVerified<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getCreatedOsAccountsCount<br>Deprecated version: N/A|Method or attribute name: getCreatedOsAccountsCount<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getCreatedOsAccountsCount<br>Deprecated version: N/A|Method or attribute name: getCreatedOsAccountsCount<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdFromProcess<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdFromProcess<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdFromProcess<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdFromProcess<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdFromUid<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdFromUid<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdFromUid<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdFromUid<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdFromDomain<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdFromDomain<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdFromDomain<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdFromDomain<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountAllConstraints<br>Deprecated version: N/A|Method or attribute name: getOsAccountAllConstraints<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountAllConstraints<br>Deprecated version: N/A|Method or attribute name: getOsAccountAllConstraints<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: queryActivatedOsAccountIds<br>Deprecated version: N/A|Method or attribute name: queryActivatedOsAccountIds<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: queryActivatedOsAccountIds<br>Deprecated version: N/A|Method or attribute name: queryActivatedOsAccountIds<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: queryCurrentOsAccount<br>Deprecated version: N/A|Method or attribute name: queryCurrentOsAccount<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: queryCurrentOsAccount<br>Deprecated version: N/A|Method or attribute name: queryCurrentOsAccount<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountTypeFromProcess<br>Deprecated version: N/A|Method or attribute name: getOsAccountTypeFromProcess<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountTypeFromProcess<br>Deprecated version: N/A|Method or attribute name: getOsAccountTypeFromProcess<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getDistributedVirtualDeviceId<br>Deprecated version: N/A|Method or attribute name: getDistributedVirtualDeviceId<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getDistributedVirtualDeviceId<br>Deprecated version: N/A|Method or attribute name: getDistributedVirtualDeviceId<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdBySerialNumber<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdBySerialNumber<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getOsAccountLocalIdBySerialNumber<br>Deprecated version: N/A|Method or attribute name: getOsAccountLocalIdBySerialNumber<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getSerialNumberByOsAccountLocalId<br>Deprecated version: N/A|Method or attribute name: getSerialNumberByOsAccountLocalId<br>Deprecated version: 9<br>New API: osAccount.AccountManager|@ohos.account.osAccount.d.ts|
|Deprecated version changed|Method or attribute name: getSerialNumberByOsAccountLocalId<br>Deprecated version: N/A|Method or attribute name: getSerialNumberByOsAccountLocalId<br>Deprecated version: 9|@ohos.account.osAccount.d.ts|
|Permission added|Method or attribute name: isOsAccountVerified<br>Permission: N/A|Method or attribute name: isOsAccountVerified<br>Permission: ohos.permission.MANAGE_LOCAL_ACCOUNTS or ohos.permission.INTERACT_ACROSS_LOCAL_ACCOUNTS.|@ohos.account.osAccount.d.ts|
