| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: sendUpdateCellLocationRequest<br>Function name: function sendUpdateCellLocationRequest(slotId?: number): Promise<void>;|@ohos.telephony.radio.d.ts|
|Initial version changed |Method or attribute name: sendUpdateCellLocationRequest<br>Initial version: 9|Method or attribute name: sendUpdateCellLocationRequest<br>Initial version: 8|@ohos.telephony.radio.d.ts|
