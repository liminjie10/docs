| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: getSync|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: get|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: get|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: get|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: setSync|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: set|@ohos.systemParameterV9.d.ts|
|Added||Module name: ohos.systemParameterV9<br>Class name: systemParameterV9<br>Method or attribute name: set|@ohos.systemParameterV9.d.ts|
