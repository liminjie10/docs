| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.multimodalInput.inputDevice<br>Class name: inputDevice<br>Method or attribute name: getDeviceList|@ohos.multimodalInput.inputDevice.d.ts|
|Added||Module name: ohos.multimodalInput.inputDevice<br>Class name: inputDevice<br>Method or attribute name: getDeviceList|@ohos.multimodalInput.inputDevice.d.ts|
|Added||Module name: ohos.multimodalInput.inputDevice<br>Class name: inputDevice<br>Method or attribute name: getDeviceInfo|@ohos.multimodalInput.inputDevice.d.ts|
|Added||Module name: ohos.multimodalInput.inputDevice<br>Class name: inputDevice<br>Method or attribute name: getDeviceInfo|@ohos.multimodalInput.inputDevice.d.ts|
|Added||Module name: ohos.multimodalInput.inputDeviceCooperate<br>Class name: EventMsg|@ohos.multimodalInput.inputDeviceCooperate.d.ts|
|Added||Module name: ohos.multimodalInput.inputDeviceCooperate<br>Class name: EventMsg<br>Method or attribute name: MSG_COOPERATE_INFO_START|@ohos.multimodalInput.inputDeviceCooperate.d.ts|
|Added||Module name: ohos.multimodalInput.inputDeviceCooperate<br>Class name: EventMsg<br>Method or attribute name: MSG_COOPERATE_INFO_SUCCESS|@ohos.multimodalInput.inputDeviceCooperate.d.ts|
|Added||Module name: ohos.multimodalInput.inputDeviceCooperate<br>Class name: EventMsg<br>Method or attribute name: MSG_COOPERATE_INFO_FAIL|@ohos.multimodalInput.inputDeviceCooperate.d.ts|
|Added||Module name: ohos.multimodalInput.inputDeviceCooperate<br>Class name: EventMsg<br>Method or attribute name: MSG_COOPERATE_STATE_ON|@ohos.multimodalInput.inputDeviceCooperate.d.ts|
|Added||Module name: ohos.multimodalInput.inputDeviceCooperate<br>Class name: EventMsg<br>Method or attribute name: MSG_COOPERATE_STATE_OFF|@ohos.multimodalInput.inputDeviceCooperate.d.ts|
|Deprecated version changed|Method or attribute name: getDeviceIds<br>Deprecated version: N/A|Method or attribute name: getDeviceIds<br>Deprecated version: 9<br>New API: ohos.multimodalInput.inputDevice|@ohos.multimodalInput.inputDevice.d.ts|
|Deprecated version changed|Method or attribute name: getDeviceIds<br>Deprecated version: N/A|Method or attribute name: getDeviceIds<br>Deprecated version: 9<br>New API: ohos.multimodalInput.inputDevice|@ohos.multimodalInput.inputDevice.d.ts|
|Deprecated version changed|Method or attribute name: getDevice<br>Deprecated version: N/A|Method or attribute name: getDevice<br>Deprecated version: 9<br>New API: ohos.multimodalInput.inputDevice|@ohos.multimodalInput.inputDevice.d.ts|
|Deprecated version changed|Method or attribute name: getDevice<br>Deprecated version: N/A|Method or attribute name: getDevice<br>Deprecated version: 9<br>New API: ohos.multimodalInput.inputDevice|@ohos.multimodalInput.inputDevice.d.ts|
