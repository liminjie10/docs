| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onBundleAdded|@ohos.EnterpriseAdminExtensionAbility.d.ts|
|Added||Module name: ohos.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onBundleRemoved|@ohos.EnterpriseAdminExtensionAbility.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: ManagedEvent|@ohos.enterpriseDeviceManager.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: ManagedEvent<br>Method or attribute name: MANAGED_EVENT_BUNDLE_ADDED|@ohos.enterpriseDeviceManager.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: ManagedEvent<br>Method or attribute name: MANAGED_EVENT_BUNDLE_REMOVED|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: enableAdmin<br>Function name: function enableAdmin(admin: Want, enterpriseInfo: EnterpriseInfo, type: AdminType, callback: AsyncCallback<void>): void;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: enableAdmin<br>Function name: function enableAdmin(admin: Want, enterpriseInfo: EnterpriseInfo, type: AdminType, userId: number, callback: AsyncCallback<void>): void;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: enableAdmin<br>Function name: function enableAdmin(admin: Want, enterpriseInfo: EnterpriseInfo, type: AdminType, userId?: number): Promise<void>;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: disableAdmin<br>Function name: function disableAdmin(admin: Want, callback: AsyncCallback<void>): void;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: disableAdmin<br>Function name: function disableAdmin(admin: Want, userId: number, callback: AsyncCallback<void>): void;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: disableAdmin<br>Function name: function disableAdmin(admin: Want, userId?: number): Promise<void>;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: disableSuperAdmin<br>Function name: function disableSuperAdmin(bundleName: String, callback: AsyncCallback<void>): void;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: disableSuperAdmin<br>Function name: function disableSuperAdmin(bundleName: String): Promise<void>;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: setEnterpriseInfo<br>Function name: function setEnterpriseInfo(admin: Want, enterpriseInfo: EnterpriseInfo, callback: AsyncCallback<void>): void;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Method or attribute name: setEnterpriseInfo<br>Function name: function setEnterpriseInfo(admin: Want, enterpriseInfo: EnterpriseInfo): Promise<void>;|@ohos.enterpriseDeviceManager.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: subscribeManagedEvent|@ohos.enterpriseDeviceManager.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: subscribeManagedEvent|@ohos.enterpriseDeviceManager.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: unsubscribeManagedEvent|@ohos.enterpriseDeviceManager.d.ts|
|Added||Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: unsubscribeManagedEvent|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: isAdminEnabled<br>model:|Method or attribute name: isAdminEnabled<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: isAdminEnabled<br>model:|Method or attribute name: isAdminEnabled<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: isAdminEnabled<br>model:|Method or attribute name: isAdminEnabled<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: getEnterpriseInfo<br>model:|Method or attribute name: getEnterpriseInfo<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: getEnterpriseInfo<br>model:|Method or attribute name: getEnterpriseInfo<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: isSuperAdmin<br>model:|Method or attribute name: isSuperAdmin<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: isSuperAdmin<br>model:|Method or attribute name: isSuperAdmin<br>model: @stage model only|@ohos.enterpriseDeviceManager.d.ts|
|Model changed|Method or attribute name: setDateTime<br>model:|Method or attribute name: setDateTime<br>model: @stage model only|DeviceSettingsManager.d.ts|
|Model changed|Method or attribute name: setDateTime<br>model:|Method or attribute name: setDateTime<br>model: @stage model only|DeviceSettingsManager.d.ts|
|Access level changed|Class name: configPolicy<br>Access level: public API|Class name: configPolicy<br>Access level: system API|@ohos.configPolicy.d.ts|
|Access level changed|Method or attribute name: isAdminEnabled<br>Access level: public API|Method or attribute name: isAdminEnabled<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isAdminEnabled<br>Access level: public API|Method or attribute name: isAdminEnabled<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isAdminEnabled<br>Access level: public API|Method or attribute name: isAdminEnabled<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: getEnterpriseInfo<br>Access level: public API|Method or attribute name: getEnterpriseInfo<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: getEnterpriseInfo<br>Access level: public API|Method or attribute name: getEnterpriseInfo<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isSuperAdmin<br>Access level: public API|Method or attribute name: isSuperAdmin<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isSuperAdmin<br>Access level: public API|Method or attribute name: isSuperAdmin<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: setDateTime<br>Access level: public API|Method or attribute name: setDateTime<br>Access level: system API|DeviceSettingsManager.d.ts|
|Access level changed|Method or attribute name: setDateTime<br>Access level: public API|Method or attribute name: setDateTime<br>Access level: system API|DeviceSettingsManager.d.ts|
|Permission changed|Method or attribute name: setDateTime<br>Permission: ohos.permission.EDM_MANAGE_DATETIME|Method or attribute name: setDateTime<br>Permission: ohos.permission.ENTERPRISE_SET_DATETIME|DeviceSettingsManager.d.ts|
|Permission changed|Method or attribute name: setDateTime<br>Permission: ohos.permission.EDM_MANAGE_DATETIME|Method or attribute name: setDateTime<br>Permission: ohos.permission.ENTERPRISE_SET_DATETIME|DeviceSettingsManager.d.ts|
|Error code added||Method or attribute name: getOneCfgFile<br>Error code: 401|@ohos.configPolicy.d.ts|
|Error code added||Method or attribute name: getCfgFiles<br>Error code: 401|@ohos.configPolicy.d.ts|
|Error code added||Method or attribute name: getCfgDirList<br>Error code: 401|@ohos.configPolicy.d.ts|
|Error code added||Method or attribute name: isAdminEnabled<br>Error code: 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: isAdminEnabled<br>Error code: 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: isAdminEnabled<br>Error code: 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: getEnterpriseInfo<br>Error code: 9200001, 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: getEnterpriseInfo<br>Error code: 9200001, 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: isSuperAdmin<br>Error code: 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: isSuperAdmin<br>Error code: 401|@ohos.enterpriseDeviceManager.d.ts|
|Error code added||Method or attribute name: setDateTime<br>Error code: 9200001, 9200002, 201, 401|DeviceSettingsManager.d.ts|
|Access level changed|Class name: configPolicy<br>Access level: public API|Class name: configPolicy<br>Access level: system API|@ohos.configPolicy.d.ts|
|Access level changed|Method or attribute name: isAdminEnabled<br>Access level: public API|Method or attribute name: isAdminEnabled<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isAdminEnabled<br>Access level: public API|Method or attribute name: isAdminEnabled<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isAdminEnabled<br>Access level: public API|Method or attribute name: isAdminEnabled<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: getEnterpriseInfo<br>Access level: public API|Method or attribute name: getEnterpriseInfo<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: getEnterpriseInfo<br>Access level: public API|Method or attribute name: getEnterpriseInfo<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isSuperAdmin<br>Access level: public API|Method or attribute name: isSuperAdmin<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: isSuperAdmin<br>Access level: public API|Method or attribute name: isSuperAdmin<br>Access level: system API|@ohos.enterpriseDeviceManager.d.ts|
|Access level changed|Method or attribute name: setDateTime<br>Access level: public API|Method or attribute name: setDateTime<br>Access level: system API|DeviceSettingsManager.d.ts|
|Access level changed|Method or attribute name: setDateTime<br>Access level: public API|Method or attribute name: setDateTime<br>Access level: system API|DeviceSettingsManager.d.ts|
