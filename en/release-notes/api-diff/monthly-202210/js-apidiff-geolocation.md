| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: on_countryCodeChange|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: off_countryCodeChange|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: enableLocation|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: enableLocation|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: disableLocation|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: disableLocation|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: getCountryCode|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: getCountryCode|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: enableLocationMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: enableLocationMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: disableLocationMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: disableLocationMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: setMockedLocations|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: setMockedLocations|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: enableReverseGeocodingMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: enableReverseGeocodingMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: disableReverseGeocodingMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: disableReverseGeocodingMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: setReverseGeocodingMockInfo|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: setReverseGeocodingMockInfo|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: isLocationPrivacyConfirmed|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: isLocationPrivacyConfirmed|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: setLocationPrivacyConfirmStatus|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: geoLocationManager<br>Method or attribute name: setLocationPrivacyConfirmStatus|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeocodingMockInfo|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeocodingMockInfo<br>Method or attribute name: location|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeocodingMockInfo<br>Method or attribute name: geoAddress|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationMockConfig|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationMockConfig<br>Method or attribute name: timeInterval|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationMockConfig<br>Method or attribute name: locations|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo<br>Method or attribute name: satellitesNumber|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo<br>Method or attribute name: satelliteIds|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo<br>Method or attribute name: carrierToNoiseDensitys|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo<br>Method or attribute name: altitudes|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo<br>Method or attribute name: azimuths|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: SatelliteStatusInfo<br>Method or attribute name: carrierFrequencies|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CachedGnssLocationsRequest|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CachedGnssLocationsRequest<br>Method or attribute name: reportingPeriodSec|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CachedGnssLocationsRequest<br>Method or attribute name: wakeUpCacheQueueFull|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeofenceRequest|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeofenceRequest<br>Method or attribute name: priority|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeofenceRequest<br>Method or attribute name: scenario|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeofenceRequest<br>Method or attribute name: geofence|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Geofence|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Geofence<br>Method or attribute name: latitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Geofence<br>Method or attribute name: longitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Geofence<br>Method or attribute name: radius|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Geofence<br>Method or attribute name: expiration|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeoCodeRequest|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeoCodeRequest<br>Method or attribute name: locale|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeoCodeRequest<br>Method or attribute name: latitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeoCodeRequest<br>Method or attribute name: longitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: ReverseGeoCodeRequest<br>Method or attribute name: maxItems|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: locale|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: description|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: maxItems|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: minLatitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: minLongitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: maxLatitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoCodeRequest<br>Method or attribute name: maxLongitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: latitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: longitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: locale|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: placeName|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: countryCode|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: countryName|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: administrativeArea|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: subAdministrativeArea|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: locality|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: subLocality|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: roadName|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: subRoadName|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: premises|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: postalCode|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: phoneNumber|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: addressUrl|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: descriptions|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: descriptionsSize|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: GeoAddress<br>Method or attribute name: isFromMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequest|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequest<br>Method or attribute name: priority|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequest<br>Method or attribute name: scenario|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequest<br>Method or attribute name: timeInterval|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequest<br>Method or attribute name: distanceInterval|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequest<br>Method or attribute name: maxAccuracy|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CurrentLocationRequest|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CurrentLocationRequest<br>Method or attribute name: priority|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CurrentLocationRequest<br>Method or attribute name: scenario|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CurrentLocationRequest<br>Method or attribute name: maxAccuracy|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CurrentLocationRequest<br>Method or attribute name: timeoutMs|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: latitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: longitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: altitude|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: accuracy|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: speed|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: timeStamp|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: direction|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: timeSinceBoot|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: additions|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: additionSize|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: Location<br>Method or attribute name: isFromMock|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestPriority|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestPriority<br>Method or attribute name: UNSET|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestPriority<br>Method or attribute name: ACCURACY|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestPriority<br>Method or attribute name: LOW_POWER|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestPriority<br>Method or attribute name: FIRST_FIX|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario<br>Method or attribute name: UNSET|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario<br>Method or attribute name: NAVIGATION|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario<br>Method or attribute name: TRAJECTORY_TRACKING|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario<br>Method or attribute name: CAR_HAILING|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario<br>Method or attribute name: DAILY_LIFE_SERVICE|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationRequestScenario<br>Method or attribute name: NO_POWER|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationPrivacyType|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationPrivacyType<br>Method or attribute name: OTHERS|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationPrivacyType<br>Method or attribute name: STARTUP|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationPrivacyType<br>Method or attribute name: CORE_LOCATION|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationCommand|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationCommand<br>Method or attribute name: scenario|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: LocationCommand<br>Method or attribute name: command|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCode|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCode<br>Method or attribute name: country|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCode<br>Method or attribute name: type|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCodeType|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_LOCALE|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_SIM|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_LOCATION|@ohos.geoLocationManager.d.ts|
|Added||Module name: ohos.geoLocationManager<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_NETWORK|@ohos.geoLocationManager.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: on_countryCodeChange||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: off_countryCodeChange||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: getCountryCode||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: getCountryCode||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: enableLocationMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: enableLocationMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: disableLocationMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: disableLocationMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: setMockedLocations||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: setMockedLocations||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: enableReverseGeocodingMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: enableReverseGeocodingMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: disableReverseGeocodingMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: disableReverseGeocodingMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: setReverseGeocodingMockInfo||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: setReverseGeocodingMockInfo||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: ReverseGeocodingMockInfo||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: ReverseGeocodingMockInfo<br>Method or attribute name: location||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: ReverseGeocodingMockInfo<br>Method or attribute name: geoAddress||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: LocationMockConfig||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: LocationMockConfig<br>Method or attribute name: timeInterval||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: LocationMockConfig<br>Method or attribute name: locations||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: isLocationPrivacyConfirmed||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: isLocationPrivacyConfirmed||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: setLocationPrivacyConfirmStatus||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: geolocation<br>Method or attribute name: setLocationPrivacyConfirmStatus||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: GeoAddress<br>Method or attribute name: isFromMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: Location<br>Method or attribute name: isFromMock||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: GeoLocationErrorCode<br>Method or attribute name: NOT_SUPPORTED||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: GeoLocationErrorCode<br>Method or attribute name: QUERY_COUNTRY_CODE_ERROR||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCode||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCode<br>Method or attribute name: country||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCode<br>Method or attribute name: type||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCodeType||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_LOCALE||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_SIM||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_LOCATION||@ohos.geolocation.d.ts|
|Deleted||Module name: ohos.geolocation<br>Class name: CountryCodeType<br>Method or attribute name: COUNTRY_CODE_FROM_NETWORK||@ohos.geolocation.d.ts|
