| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.file.fs<br>Class name: fileIo|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: READ_ONLY|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: WRITE_ONLY|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: READ_WRITE|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: CREATE|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: TRUNC|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: APPEND|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: NONBLOCK|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: DIR|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: NOFOLLOW|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: OpenMode<br>Method or attribute name: SYNC|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: open|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: open|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: open|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: openSync|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: read|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: read|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: read|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: readSync|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: stat|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: stat|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: statSync|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: truncate|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: truncate|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: truncate|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: truncateSync|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: write|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: write|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: write|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: fileIo<br>Method or attribute name: writeSync|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: File|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: File<br>Method or attribute name: fd|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: ino|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: mode|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: uid|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: gid|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: size|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: atime|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: mtime|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: ctime|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isBlockDevice|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isCharacterDevice|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isDirectory|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isFIFO|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isFile|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isSocket|@ohos.file.fs.d.ts|
|Added||Module name: ohos.file.fs<br>Class name: Stat<br>Method or attribute name: isSymbolicLink|@ohos.file.fs.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: userFileManager|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: userFileManager<br>Method or attribute name: getUserFileMgr|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileType|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileType<br>Method or attribute name: IMAGE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileType<br>Method or attribute name: VIDEO|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileType<br>Method or attribute name: AUDIO|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: uri|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: fileType|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: displayName|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: get|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: set|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: commitModify|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: commitModify|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: open|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: open|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: close|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: close|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: getThumbnail|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: getThumbnail|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: getThumbnail|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: favorite|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FileAsset<br>Method or attribute name: favorite|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: URI|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: DISPLAY_NAME|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: DATE_ADDED|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: DATE_MODIFIED|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: TITLE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: ARTIST|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: AUDIOALBUM|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: DURATION|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AudioKey<br>Method or attribute name: FAVORITE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: URI|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: FILE_TYPE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: DISPLAY_NAME|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: DATE_ADDED|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: DATE_MODIFIED|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: TITLE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: DURATION|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: WIDTH|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: HEIGHT|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: DATE_TAKEN|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: ORIENTATION|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: ImageVideoKey<br>Method or attribute name: FAVORITE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumKey|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumKey<br>Method or attribute name: URI|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumKey<br>Method or attribute name: FILE_TYPE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumKey<br>Method or attribute name: ALBUM_NAME|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumKey<br>Method or attribute name: DATE_ADDED|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumKey<br>Method or attribute name: DATE_MODIFIED|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchOptions|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchOptions<br>Method or attribute name: fetchColumns|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchOptions<br>Method or attribute name: predicates|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumFetchOptions|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AlbumFetchOptions<br>Method or attribute name: predicates|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getCount|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: isAfterLast|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: close|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getFirstObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getFirstObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getNextObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getNextObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getLastObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getLastObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getPositionObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: FetchResult<br>Method or attribute name: getPositionObject|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: albumName|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: albumUri|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: dateModified|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: count|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: coverUri|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: getPhotoAssets|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: AbsAlbum<br>Method or attribute name: getPhotoAssets|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: Album|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: Album<br>Method or attribute name: commitModify|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: Album<br>Method or attribute name: commitModify|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getPhotoAssets|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getPhotoAssets|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: createPhotoAsset|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: createPhotoAsset|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: createPhotoAsset|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getPhotoAlbums|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getPhotoAlbums|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getPrivateAlbum|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getPrivateAlbum|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getAudioAssets|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getAudioAssets|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: delete|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: delete|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: on_deviceChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: on_albumChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: on_imageChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: on_audioChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: on_videoChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: on_remoteFileChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: off_deviceChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: off_albumChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: off_imageChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: off_audioChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: off_videoChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: off_remoteFileChange|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getActivePeers|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getActivePeers|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getAllPeers|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: getAllPeers|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: release|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: UserFileManager<br>Method or attribute name: release|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PeerInfo|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PeerInfo<br>Method or attribute name: deviceName|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PeerInfo<br>Method or attribute name: networkId|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PeerInfo<br>Method or attribute name: isOnline|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbumType|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbumType<br>Method or attribute name: TYPE_FAVORITE|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbumType<br>Method or attribute name: TYPE_TRASH|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbum|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbum<br>Method or attribute name: delete|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbum<br>Method or attribute name: delete|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbum<br>Method or attribute name: recover|@ohos.filemanagement.userFileManager.d.ts|
|Added||Module name: ohos.filemanagement.userFileManager<br>Class name: PrivateAlbum<br>Method or attribute name: recover|@ohos.filemanagement.userFileManager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: userfile_manager||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: userfile_manager<br>Method or attribute name: getUserFileMgr||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: userfile_manager<br>Method or attribute name: getUserFileMgr||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaType||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaType<br>Method or attribute name: FILE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaType<br>Method or attribute name: IMAGE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaType<br>Method or attribute name: VIDEO||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaType<br>Method or attribute name: AUDIO||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: uri||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: mediaType||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: displayName||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: isDirectory||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: isDirectory||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: commitModify||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: commitModify||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: open||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: open||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: close||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: close||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: getThumbnail||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: getThumbnail||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: getThumbnail||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: favorite||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: favorite||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: isFavorite||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: isFavorite||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: trash||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: trash||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: isTrash||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileAsset<br>Method or attribute name: isTrash||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey<br>Method or attribute name: URI||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey<br>Method or attribute name: RELATIVE_PATH||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey<br>Method or attribute name: DISPLAY_NAME||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey<br>Method or attribute name: DATE_ADDED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey<br>Method or attribute name: DATE_MODIFIED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FileKey<br>Method or attribute name: TITLE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: URI||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: RELATIVE_PATH||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: DISPLAY_NAME||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: DATE_ADDED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: DATE_MODIFIED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: TITLE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: ARTIST||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: AUDIOALBUM||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AudioKey<br>Method or attribute name: DURATION||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: URI||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: RELATIVE_PATH||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: DISPLAY_NAME||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: DATE_ADDED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: DATE_MODIFIED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: TITLE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: DURATION||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: WIDTH||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: HEIGHT||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: ImageVideoKey<br>Method or attribute name: DATE_TAKEN||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AlbumKey||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AlbumKey<br>Method or attribute name: URI||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AlbumKey<br>Method or attribute name: RELATIVE_PATH||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AlbumKey<br>Method or attribute name: DISPLAY_NAME||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AlbumKey<br>Method or attribute name: DATE_ADDED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: AlbumKey<br>Method or attribute name: DATE_MODIFIED||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaFetchOptions||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaFetchOptions<br>Method or attribute name: selections||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: MediaFetchOptions<br>Method or attribute name: selectionArgs||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getCount||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: isAfterLast||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: close||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getFirstObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getFirstObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getNextObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getNextObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getLastObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getLastObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getPositionObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: FetchFileResult<br>Method or attribute name: getPositionObject||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: albumName||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: albumUri||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: dateModified||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: count||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: relativePath||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: coverUri||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: commitModify||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: commitModify||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Album<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType<br>Method or attribute name: DIR_CAMERA||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType<br>Method or attribute name: DIR_VIDEO||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType<br>Method or attribute name: DIR_IMAGE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType<br>Method or attribute name: DIR_AUDIO||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType<br>Method or attribute name: DIR_DOCUMENTS||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: DirectoryType<br>Method or attribute name: DIR_DOWNLOAD||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getPublicDirectory||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getPublicDirectory||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_deviceChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_albumChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_imageChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_audioChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_videoChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_fileChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: on_remoteFileChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_deviceChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_albumChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_imageChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_audioChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_videoChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_fileChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: off_remoteFileChange||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: createAsset||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: createAsset||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: deleteAsset||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: deleteAsset||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getAlbums||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getAlbums||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getPrivateAlbum||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getPrivateAlbum||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getActivePeers||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getActivePeers||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getAllPeers||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: getAllPeers||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: release||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: UserFileManager<br>Method or attribute name: release||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Size||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Size<br>Method or attribute name: width||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: Size<br>Method or attribute name: height||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: PeerInfo||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: PeerInfo<br>Method or attribute name: deviceName||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: PeerInfo<br>Method or attribute name: networkId||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: PeerInfo<br>Method or attribute name: isOnline||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: VirtualAlbumType||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: VirtualAlbumType<br>Method or attribute name: TYPE_FAVORITE||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: VirtualAlbumType<br>Method or attribute name: TYPE_TRASH||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: VirtualAlbum||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: VirtualAlbum<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.filemanagement.userfile_manager<br>Class name: VirtualAlbum<br>Method or attribute name: getFileAssets||@ohos.filemanagement.userfile_manager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: listFile||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: listFile||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: listFile||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: getRoot||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: getRoot||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: getRoot||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: createFile||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: createFile||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: filemanager<br>Method or attribute name: createFile||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo<br>Method or attribute name: name||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo<br>Method or attribute name: path||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo<br>Method or attribute name: type||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo<br>Method or attribute name: size||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo<br>Method or attribute name: addedTime||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: FileInfo<br>Method or attribute name: modifiedTime||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: DevInfo||@ohos.fileManager.d.ts|
|Deleted||Module name: ohos.fileManager<br>Class name: DevInfo<br>Method or attribute name: name||@ohos.fileManager.d.ts|
|Deprecated version changed|Method or attribute name: ftruncate<br>Deprecated version: N/A|Method or attribute name: ftruncate<br>Deprecated version: 9<br>New API: ohos.file.fs.truncate |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: ftruncate<br>Deprecated version: N/A|Method or attribute name: ftruncate<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: ftruncate<br>Deprecated version: N/A|Method or attribute name: ftruncate<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: ftruncateSync<br>Deprecated version: N/A|Method or attribute name: ftruncateSync<br>Deprecated version: 9<br>New API: ohos.file.fs.truncateSync |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: fstat<br>Deprecated version: N/A|Method or attribute name: fstat<br>Deprecated version: 9<br>New API: ohos.file.fs.stat |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: fstat<br>Deprecated version: N/A|Method or attribute name: fstat<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: fstatSync<br>Deprecated version: N/A|Method or attribute name: fstatSync<br>Deprecated version: 9<br>New API: ohos.file.fs.statSync |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: open<br>Deprecated version: N/A|Method or attribute name: open<br>Deprecated version: 9<br>New API: ohos.file.fs.open |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: open<br>Deprecated version: N/A|Method or attribute name: open<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: open<br>Deprecated version: N/A|Method or attribute name: open<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: open<br>Deprecated version: N/A|Method or attribute name: open<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: openSync<br>Deprecated version: N/A|Method or attribute name: openSync<br>Deprecated version: 9<br>New API: ohos.file.fs.openSync |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: read<br>Deprecated version: N/A|Method or attribute name: read<br>Deprecated version: 9<br>New API: ohos.file.fs.read |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: read<br>Deprecated version: N/A|Method or attribute name: read<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: read<br>Deprecated version: N/A|Method or attribute name: read<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: readSync<br>Deprecated version: N/A|Method or attribute name: readSync<br>Deprecated version: 9<br>New API: ohos.file.fs.readSync |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: stat<br>Deprecated version: N/A|Method or attribute name: stat<br>Deprecated version: 9<br>New API: ohos.file.fs.stat |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: stat<br>Deprecated version: N/A|Method or attribute name: stat<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: statSync<br>Deprecated version: N/A|Method or attribute name: statSync<br>Deprecated version: 9<br>New API: ohos.file.fs.statSync |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: truncate<br>Deprecated version: N/A|Method or attribute name: truncate<br>Deprecated version: 9<br>New API: ohos.file.fs.truncate |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: truncate<br>Deprecated version: N/A|Method or attribute name: truncate<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: truncate<br>Deprecated version: N/A|Method or attribute name: truncate<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: truncateSync<br>Deprecated version: N/A|Method or attribute name: truncateSync<br>Deprecated version: 9<br>New API: ohos.file.fs.truncateSync |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: write<br>Deprecated version: N/A|Method or attribute name: write<br>Deprecated version: 9<br>New API: ohos.file.fs.write |@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: write<br>Deprecated version: N/A|Method or attribute name: write<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: write<br>Deprecated version: N/A|Method or attribute name: write<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Deprecated version changed|Method or attribute name: writeSync<br>Deprecated version: N/A|Method or attribute name: writeSync<br>Deprecated version: 9<br>New API: ohos.file.fs.writeSync |@ohos.fileio.d.ts|
|Deprecated version changed|Class name: Stat<br>Deprecated version: N/A|Class name: Stat<br>Deprecated version: 9<br>New API: ohos.file.fs.Stat |@ohos.fileio.d.ts|
|Deprecated version changed|Class name: ReadOut<br>Deprecated version: N/A|Class name: ReadOut<br>Deprecated version: 9|@ohos.fileio.d.ts|
|Permission changed|Method or attribute name: getFileAccessAbilityInfo<br>Permission: ohos.permission.FILE_ACCESS_MANAGER|Method or attribute name: getFileAccessAbilityInfo<br>Permission: ohos.permission.FILE_ACCESS_MANAGER and ohos.permission.GET_BUNDLE_INFO_PRIVILEGED|@ohos.data.fileAccess.d.ts|
|Permission changed|Method or attribute name: getFileAccessAbilityInfo<br>Permission: ohos.permission.FILE_ACCESS_MANAGER|Method or attribute name: getFileAccessAbilityInfo<br>Permission: ohos.permission.FILE_ACCESS_MANAGER and ohos.permission.GET_BUNDLE_INFO_PRIVILEGED|@ohos.data.fileAccess.d.ts|
|Permission changed|Method or attribute name: createFileAccessHelper<br>Permission: ohos.permission.FILE_ACCESS_MANAGER|Method or attribute name: createFileAccessHelper<br>Permission: ohos.permission.FILE_ACCESS_MANAGER and ohos.permission.GET_BUNDLE_INFO_PRIVILEGED|@ohos.data.fileAccess.d.ts|
|Permission changed|Method or attribute name: createFileAccessHelper<br>Permission: ohos.permission.FILE_ACCESS_MANAGER|Method or attribute name: createFileAccessHelper<br>Permission: ohos.permission.FILE_ACCESS_MANAGER and ohos.permission.GET_BUNDLE_INFO_PRIVILEGED|@ohos.data.fileAccess.d.ts|
