| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: EnterpriseInfo|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: EnterpriseInfo<br>Method or attribute name: name|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: EnterpriseInfo<br>Method or attribute name: description|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: AdminType|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: AdminType<br>Method or attribute name: ADMIN_TYPE_NORMAL|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: AdminType<br>Method or attribute name: ADMIN_TYPE_SUPER|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: ManagedEvent|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: ManagedEvent<br>Method or attribute name: MANAGED_EVENT_BUNDLE_ADDED|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: ManagedEvent<br>Method or attribute name: MANAGED_EVENT_BUNDLE_REMOVED|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: enableAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: enableAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: enableAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: disableAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: disableAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: disableAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: disableSuperAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: disableSuperAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: isAdminEnabled|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: isAdminEnabled|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: isAdminEnabled|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: getEnterpriseInfo|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: getEnterpriseInfo|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: setEnterpriseInfo|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: setEnterpriseInfo|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: isSuperAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: isSuperAdmin|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: subscribeManagedEvent|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: subscribeManagedEvent|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: unsubscribeManagedEvent|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.adminManager<br>Class name: adminManager<br>Method or attribute name: unsubscribeManagedEvent|@ohos.enterprise.adminManager.d.ts|
|Added||Module name: ohos.enterprise.dateTimeManager<br>Class name: dateTimeManager|@ohos.enterprise.dateTimeManager.d.ts|
|Added||Module name: ohos.enterprise.dateTimeManager<br>Class name: dateTimeManager<br>Method or attribute name: setDateTime|@ohos.enterprise.dateTimeManager.d.ts|
|Added||Module name: ohos.enterprise.dateTimeManager<br>Class name: dateTimeManager<br>Method or attribute name: setDateTime|@ohos.enterprise.dateTimeManager.d.ts|
|Added||Module name: ohos.enterprise.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility|@ohos.enterprise.EnterpriseAdminExtensionAbility.d.ts|
|Added||Module name: ohos.enterprise.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onAdminEnabled|@ohos.enterprise.EnterpriseAdminExtensionAbility.d.ts|
|Added||Module name: ohos.enterprise.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onAdminDisabled|@ohos.enterprise.EnterpriseAdminExtensionAbility.d.ts|
|Added||Module name: ohos.enterprise.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onBundleAdded|@ohos.enterprise.EnterpriseAdminExtensionAbility.d.ts|
|Added||Module name: ohos.enterprise.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onBundleRemoved|@ohos.enterprise.EnterpriseAdminExtensionAbility.d.ts|
|Deleted|Module name: ohos.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility||@ohos.EnterpriseAdminExtensionAbility.d.ts|
|Deleted|Module name: ohos.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onAdminEnabled||@ohos.EnterpriseAdminExtensionAbility.d.ts|
|Deleted|Module name: ohos.EnterpriseAdminExtensionAbility<br>Class name: EnterpriseAdminExtensionAbility<br>Method or attribute name: onAdminDisabled||@ohos.EnterpriseAdminExtensionAbility.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: EnterpriseInfo||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: EnterpriseInfo<br>Method or attribute name: name||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: EnterpriseInfo<br>Method or attribute name: description||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: AdminType||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: AdminType<br>Method or attribute name: ADMIN_TYPE_NORMAL||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: AdminType<br>Method or attribute name: ADMIN_TYPE_SUPER||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: enableAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: enableAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: enableAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: disableAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: disableAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: disableAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: disableSuperAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: disableSuperAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: isAdminEnabled||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: isAdminEnabled||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: isAdminEnabled||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: getEnterpriseInfo||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: getEnterpriseInfo||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: setEnterpriseInfo||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: setEnterpriseInfo||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: isSuperAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: isSuperAdmin||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: getDeviceSettingsManager||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: ohos.enterpriseDeviceManager<br>Class name: enterpriseDeviceManager<br>Method or attribute name: getDeviceSettingsManager||@ohos.enterpriseDeviceManager.d.ts|
|Deleted|Module name: DeviceSettingsManager<br>Class name: DeviceSettingsManager||DeviceSettingsManager.d.ts|
|Deleted|Module name: DeviceSettingsManager<br>Class name: DeviceSettingsManager<br>Method or attribute name: setDateTime||DeviceSettingsManager.d.ts|
|Deleted|Module name: DeviceSettingsManager<br>Class name: DeviceSettingsManager<br>Method or attribute name: setDateTime||DeviceSettingsManager.d.ts|
|Access level changed|Class name: configPolicy<br>Access level: public API|Class name: configPolicy<br>Access level: system  API|@ohos.configPolicy.d.ts|
|Error code added||Method or attribute name: getOneCfgFile<br>Error code: 401|@ohos.configPolicy.d.ts|
|Error code added||Method or attribute name: getCfgFiles<br>Error code: 401|@ohos.configPolicy.d.ts|
|Error code added||Method or attribute name: getCfgDirList<br>Error code: 401|@ohos.configPolicy.d.ts|
|Access level changed|Class name: configPolicy<br>Access level: public API|Class name: configPolicy<br>Access level: system API|@ohos.configPolicy.d.ts|
