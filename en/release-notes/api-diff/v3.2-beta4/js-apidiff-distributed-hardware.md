| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.distributedHardware.deviceManager<br>Class name: DeviceManager<br>Method or attribute name: setUserOperation|@ohos.distributedHardware.deviceManager.d.ts|
|Added||Module name: ohos.distributedHardware.deviceManager<br>Class name: DeviceManager<br>Method or attribute name: on_uiStateChange|@ohos.distributedHardware.deviceManager.d.ts|
|Added||Module name: ohos.distributedHardware.deviceManager<br>Class name: DeviceManager<br>Method or attribute name: off_uiStateChange|@ohos.distributedHardware.deviceManager.d.ts|
