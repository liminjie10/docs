| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.display<br>Class name: display<br>Method or attribute name: getAllDisplays|@ohos.display.d.ts|
|Added||Module name: ohos.display<br>Class name: display<br>Method or attribute name: getAllDisplays|@ohos.display.d.ts|
|Added||Module name: ohos.window<br>Class name: Configuration|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Configuration<br>Method or attribute name: name|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Configuration<br>Method or attribute name: windowType|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Configuration<br>Method or attribute name: ctx|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Configuration<br>Method or attribute name: displayId|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Configuration<br>Method or attribute name: parentId|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: window<br>Method or attribute name: createWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: window<br>Method or attribute name: createWindow|@ohos.window.d.ts|
|Added||Method or attribute name: create<br>Function name: function create(ctx: BaseContext, id: string, type: WindowType): Promise<Window>;|@ohos.window.d.ts|
|Added||Method or attribute name: create<br>Function name: function create(ctx: BaseContext, id: string, type: WindowType, callback: AsyncCallback<Window>): void;|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: window<br>Method or attribute name: findWindow|@ohos.window.d.ts|
|Added||Method or attribute name: getTopWindow<br>Function name: function getTopWindow(ctx: BaseContext): Promise<Window>;|@ohos.window.d.ts|
|Added||Method or attribute name: getTopWindow<br>Function name: function getTopWindow(ctx: BaseContext, callback: AsyncCallback<Window>): void;|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: window<br>Method or attribute name: getLastWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: window<br>Method or attribute name: getLastWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: showWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: showWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: destroyWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: destroyWindow|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: moveWindowTo|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: moveWindowTo|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: resize|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: resize|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowMode|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowMode|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: getWindowProperties|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: getWindowAvoidArea|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowLayoutFullScreen|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowLayoutFullScreen|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowSystemBarEnable|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowSystemBarEnable|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowSystemBarProperties|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowSystemBarProperties|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setUIContent|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setUIContent|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: isWindowShowing|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: isWindowSupportWideGamut|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: isWindowSupportWideGamut|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowColorSpace|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowColorSpace|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: getWindowColorSpace|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowBackgroundColor|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowBrightness|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowBrightness|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowFocusable|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowFocusable|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowKeepScreenOn|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowKeepScreenOn|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowPrivacyMode|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowPrivacyMode|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowTouchable|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: Window<br>Method or attribute name: setWindowTouchable|@ohos.window.d.ts|
|Added||Module name: ohos.window<br>Class name: WindowStage<br>Method or attribute name: getMainWindowSync|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getDefaultDisplay<br>Deprecated version: N/A|Method or attribute name: getDefaultDisplay<br>Deprecated version: 9<br>New API: ohos.display|@ohos.display.d.ts|
|Deprecated version changed|Method or attribute name: getDefaultDisplay<br>Deprecated version: N/A|Method or attribute name: getDefaultDisplay<br>Deprecated version: 9<br>New API: ohos.display|@ohos.display.d.ts|
|Deprecated version changed|Method or attribute name: getAllDisplay<br>Deprecated version: N/A|Method or attribute name: getAllDisplay<br>Deprecated version: 9<br>New API: ohos.display|@ohos.display.d.ts|
|Deprecated version changed|Method or attribute name: getAllDisplay<br>Deprecated version: N/A|Method or attribute name: getAllDisplay<br>Deprecated version: 9<br>New API: ohos.display|@ohos.display.d.ts|
|Deprecated version changed|Method or attribute name: create<br>Deprecated version: N/A|Method or attribute name: create<br>Deprecated version: 9<br>New API: ohos.window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: create<br>Deprecated version: N/A|Method or attribute name: create<br>Deprecated version: 9<br>New API: ohos.window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: find<br>Deprecated version: N/A|Method or attribute name: find<br>Deprecated version: 9<br>New API: ohos.window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: find<br>Deprecated version: N/A|Method or attribute name: find<br>Deprecated version: 9<br>New API: ohos.window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getTopWindow<br>Deprecated version: N/A|Method or attribute name: getTopWindow<br>Deprecated version: 9<br>New API: ohos.window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getTopWindow<br>Deprecated version: N/A|Method or attribute name: getTopWindow<br>Deprecated version: 9<br>New API: ohos.window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: show<br>Deprecated version: N/A|Method or attribute name: show<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: show<br>Deprecated version: N/A|Method or attribute name: show<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: destroy<br>Deprecated version: N/A|Method or attribute name: destroy<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: destroy<br>Deprecated version: N/A|Method or attribute name: destroy<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: moveTo<br>Deprecated version: N/A|Method or attribute name: moveTo<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: moveTo<br>Deprecated version: N/A|Method or attribute name: moveTo<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: resetSize<br>Deprecated version: N/A|Method or attribute name: resetSize<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: resetSize<br>Deprecated version: N/A|Method or attribute name: resetSize<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getProperties<br>Deprecated version: N/A|Method or attribute name: getProperties<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getProperties<br>Deprecated version: N/A|Method or attribute name: getProperties<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getAvoidArea<br>Deprecated version: N/A|Method or attribute name: getAvoidArea<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getAvoidArea<br>Deprecated version: N/A|Method or attribute name: getAvoidArea<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setFullScreen<br>Deprecated version: N/A|Method or attribute name: setFullScreen<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setFullScreen<br>Deprecated version: N/A|Method or attribute name: setFullScreen<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setLayoutFullScreen<br>Deprecated version: N/A|Method or attribute name: setLayoutFullScreen<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setLayoutFullScreen<br>Deprecated version: N/A|Method or attribute name: setLayoutFullScreen<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setSystemBarEnable<br>Deprecated version: N/A|Method or attribute name: setSystemBarEnable<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setSystemBarEnable<br>Deprecated version: N/A|Method or attribute name: setSystemBarEnable<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setSystemBarProperties<br>Deprecated version: N/A|Method or attribute name: setSystemBarProperties<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setSystemBarProperties<br>Deprecated version: N/A|Method or attribute name: setSystemBarProperties<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: loadContent<br>Deprecated version: N/A|Method or attribute name: loadContent<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: loadContent<br>Deprecated version: N/A|Method or attribute name: loadContent<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: isShowing<br>Deprecated version: N/A|Method or attribute name: isShowing<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: isShowing<br>Deprecated version: N/A|Method or attribute name: isShowing<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: isSupportWideGamut<br>Deprecated version: N/A|Method or attribute name: isSupportWideGamut<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: isSupportWideGamut<br>Deprecated version: N/A|Method or attribute name: isSupportWideGamut<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setColorSpace<br>Deprecated version: N/A|Method or attribute name: setColorSpace<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setColorSpace<br>Deprecated version: N/A|Method or attribute name: setColorSpace<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getColorSpace<br>Deprecated version: N/A|Method or attribute name: getColorSpace<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: getColorSpace<br>Deprecated version: N/A|Method or attribute name: getColorSpace<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setBackgroundColor<br>Deprecated version: N/A|Method or attribute name: setBackgroundColor<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setBackgroundColor<br>Deprecated version: N/A|Method or attribute name: setBackgroundColor<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setBrightness<br>Deprecated version: N/A|Method or attribute name: setBrightness<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setBrightness<br>Deprecated version: N/A|Method or attribute name: setBrightness<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setFocusable<br>Deprecated version: N/A|Method or attribute name: setFocusable<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setFocusable<br>Deprecated version: N/A|Method or attribute name: setFocusable<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setKeepScreenOn<br>Deprecated version: N/A|Method or attribute name: setKeepScreenOn<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setKeepScreenOn<br>Deprecated version: N/A|Method or attribute name: setKeepScreenOn<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setPrivacyMode<br>Deprecated version: N/A|Method or attribute name: setPrivacyMode<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setPrivacyMode<br>Deprecated version: N/A|Method or attribute name: setPrivacyMode<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setTouchable<br>Deprecated version: N/A|Method or attribute name: setTouchable<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Deprecated version changed|Method or attribute name: setTouchable<br>Deprecated version: N/A|Method or attribute name: setTouchable<br>Deprecated version: 9<br>New API: ohos.window.Window|@ohos.window.d.ts|
|Permission changed|Method or attribute name: createVirtualScreen<br>Permission: ohos.permission.CAPTURE_SCREEN. if VirtualScreenOption.surfaceId is valid|Method or attribute name: createVirtualScreen<br>Permission: ohos.permission.CAPTURE_SCREEN|@ohos.screen.d.ts|
|Permission changed|Method or attribute name: createVirtualScreen<br>Permission: ohos.permission.CAPTURE_SCREEN. if VirtualScreenOption.surfaceId is valid|Method or attribute name: createVirtualScreen<br>Permission: ohos.permission.CAPTURE_SCREEN|@ohos.screen.d.ts|
