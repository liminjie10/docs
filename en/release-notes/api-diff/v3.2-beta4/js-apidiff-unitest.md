| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: bundleName<br>Function name: bundleName?: string;|@ohos.uitest.d.ts|
|Added||Method or attribute name: title<br>Function name: title?: string;|@ohos.uitest.d.ts|
|Added||Method or attribute name: focused<br>Function name: focused?: boolean;|@ohos.uitest.d.ts|
|Added||Method or attribute name: actived<br>Function name: actived?: boolean;|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: text|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: id|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: type|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: clickable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: longClickable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: scrollable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: enabled|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: focused|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: selected|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: checked|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: checkable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: isBefore|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: On<br>Method or attribute name: isAfter|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: click|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: doubleClick|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: longClick|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: getId|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: getText|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: getType|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isClickable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isLongClickable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isScrollable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isEnabled|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isFocused|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isSelected|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isChecked|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: isCheckable|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: inputText|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: clearText|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: scrollToTop|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: scrollToBottom|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: scrollSearch|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: getBounds|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: getBoundsCenter|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: dragTo|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: pinchOut|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Component<br>Method or attribute name: pinchIn|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: create|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: delayMs|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: findComponent|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: findWindow|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: waitForComponent|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: findComponents|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: assertComponentExist|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: pressBack|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: triggerKey|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: triggerCombineKeys|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: click|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: doubleClick|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: longClick|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: swipe|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: drag|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: screenCap|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: setDisplayRotation|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: getDisplayRotation|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: setDisplayRotationEnabled|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: getDisplaySize|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: getDisplayDensity|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: wakeUpDisplay|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: pressHome|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: waitForIdle|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: fling|@ohos.uitest.d.ts|
|Added||Module name: ohos.uitest<br>Class name: Driver<br>Method or attribute name: injectMultiPointerAction|@ohos.uitest.d.ts|
|Added||Method or attribute name: focus<br>Function name: focus(): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: moveTo<br>Function name: moveTo(x: number, y: number): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: resize<br>Function name: resize(wide: number, height: number, direction: ResizeDirection): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: split<br>Function name: split(): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: maximize<br>Function name: maximize(): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: minimize<br>Function name: minimize(): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: resume<br>Function name: resume(): Promise<void>;|@ohos.uitest.d.ts|
|Added||Method or attribute name: close<br>Function name: close(): Promise<void>;|@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: By<br>Method or attribute name: longClickable||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: By<br>Method or attribute name: checked||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: By<br>Method or attribute name: checkable||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: isLongClickable||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: isChecked||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: isCheckable||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: clearText||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: scrollToTop||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: scrollToBottom||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: getBounds||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: getBoundsCenter||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: dragTo||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: pinchOut||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiComponent<br>Method or attribute name: pinchIn||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: findWindow||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: waitForComponent||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: triggerCombineKeys||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: drag||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: setDisplayRotation||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: getDisplayRotation||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: setDisplayRotationEnabled||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: getDisplaySize||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: getDisplayDensity||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: wakeUpDisplay||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: pressHome||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: waitForIdle||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: fling||@ohos.uitest.d.ts|
|Deleted||Module name: ohos.uitest<br>Class name: UiDriver<br>Method or attribute name: injectMultiPointerAction||@ohos.uitest.d.ts|
|Deprecated version changed|Class name: By<br>Deprecated version: N/A|Class name: By<br>Deprecated version: 9<br>New API: ohos.uitest.On |@ohos.uitest.d.ts|
|Deprecated version changed|Class name: UiComponent<br>Deprecated version: N/A|Class name: UiComponent<br>Deprecated version: 9<br>New API: ohos.uitest.Component |@ohos.uitest.d.ts|
|Deprecated version changed|Class name: UiDriver<br>Deprecated version: N/A|Class name: UiDriver<br>Deprecated version: 9<br>New API: ohos.uitest.Driver |@ohos.uitest.d.ts|
