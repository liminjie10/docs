| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.faultLogger<br>Class name: FaultLogger<br>Method or attribute name: query|@ohos.faultLogger.d.ts|
|Added||Module name: ohos.faultLogger<br>Class name: FaultLogger<br>Method or attribute name: query|@ohos.faultLogger.d.ts|
|Added||Module name: ohos.hichecker<br>Class name: hichecker<br>Method or attribute name: addCheckRule|@ohos.hichecker.d.ts|
|Added||Module name: ohos.hichecker<br>Class name: hichecker<br>Method or attribute name: removeCheckRule|@ohos.hichecker.d.ts|
|Added||Module name: ohos.hichecker<br>Class name: hichecker<br>Method or attribute name: containsCheckRule|@ohos.hichecker.d.ts|
|Added||Module name: ohos.hidebug<br>Class name: hidebug<br>Method or attribute name: startJsCpuProfiling|@ohos.hidebug.d.ts|
|Added||Module name: ohos.hidebug<br>Class name: hidebug<br>Method or attribute name: stopJsCpuProfiling|@ohos.hidebug.d.ts|
|Added||Module name: ohos.hidebug<br>Class name: hidebug<br>Method or attribute name: dumpJsHeapData|@ohos.hidebug.d.ts|
|Added||Method or attribute name: getServiceDump<br>Function name: function getServiceDump(serviceid : number, fd : number, args : Array<string>) : void;|@ohos.hidebug.d.ts|
|Added||Method or attribute name: onQuery<br>Function name: onQuery: (infos: SysEventInfo[]) => void;|@ohos.hiSysEvent.d.ts|
|Added||Method or attribute name: addWatcher<br>Function name: function addWatcher(watcher: Watcher): void;|@ohos.hiSysEvent.d.ts|
|Added||Method or attribute name: removeWatcher<br>Function name: function removeWatcher(watcher: Watcher): void;|@ohos.hiSysEvent.d.ts|
|Added||Method or attribute name: query<br>Function name: function query(queryArg: QueryArg, rules: QueryRule[], querier: Querier): void;|@ohos.hiSysEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: EventType|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: EventType<br>Method or attribute name: FAULT|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: EventType<br>Method or attribute name: STATISTIC|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: EventType<br>Method or attribute name: SECURITY|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: EventType<br>Method or attribute name: BEHAVIOR|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Event|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Event<br>Method or attribute name: USER_LOGIN|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Event<br>Method or attribute name: USER_LOGOUT|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Event<br>Method or attribute name: DISTRIBUTED_SERVICE_START|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Param|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Param<br>Method or attribute name: USER_ID|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Param<br>Method or attribute name: DISTRIBUTED_SERVICE_NAME|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Param<br>Method or attribute name: DISTRIBUTED_SERVICE_INSTANCE_ID|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: configure|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: ConfigOption|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: ConfigOption<br>Method or attribute name: disable|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: ConfigOption<br>Method or attribute name: maxStorage|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventInfo|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: domain|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: name|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: eventType|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: params|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: write|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: write|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackage|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: packageId|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: row|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: size|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: data|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackageHolder|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackageHolder<br>Method or attribute name: ructor(watcherName|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackageHolder<br>Method or attribute name: setSize|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventPackageHolder<br>Method or attribute name: takeNext|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: TriggerCondition|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: TriggerCondition<br>Method or attribute name: row|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: TriggerCondition<br>Method or attribute name: size|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: TriggerCondition<br>Method or attribute name: timeOut|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventFilter|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventFilter<br>Method or attribute name: domain|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: AppEventFilter<br>Method or attribute name: eventTypes|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Watcher|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: name|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: triggerCondition|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: appEventFilters|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: onTrigger|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: addWatcher|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: removeWatcher|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Added||Module name: ohos.hiviewdfx.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: clearData|@ohos.hiviewdfx.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventInfo||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: domain||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: name||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: eventType||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventInfo<br>Method or attribute name: params||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackage||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: packageId||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: row||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: size||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackage<br>Method or attribute name: data||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackageHolder||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackageHolder<br>Method or attribute name: ructor(watcherName||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackageHolder<br>Method or attribute name: setSize||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventPackageHolder<br>Method or attribute name: takeNext||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: TriggerCondition||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: TriggerCondition<br>Method or attribute name: row||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: TriggerCondition<br>Method or attribute name: size||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: TriggerCondition<br>Method or attribute name: timeOut||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventFilter||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventFilter<br>Method or attribute name: domain||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: AppEventFilter<br>Method or attribute name: eventTypes||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: Watcher||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: name||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: triggerCondition||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: appEventFilters||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: Watcher<br>Method or attribute name: onTrigger||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: addWatcher||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: removeWatcher||@ohos.hiAppEvent.d.ts|
|Deleted |Module name: ohos.hiAppEvent<br>Class name: hiAppEvent<br>Method or attribute name: clearData||@ohos.hiAppEvent.d.ts|
|Deprecated version changed|Class name: bytrace<br>Deprecated version: N/A|Class name: bytrace<br>Deprecated version: 8<br>New API: ohos.hiTraceMeter  |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: startTrace<br>Deprecated version: N/A|Method or attribute name: startTrace<br>Deprecated version: 8<br>New API: ohos.hiTraceMeter.startTrace   |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: finishTrace<br>Deprecated version: N/A|Method or attribute name: finishTrace<br>Deprecated version: 8<br>New API: ohos.hiTraceMeter.finishTrace   |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: traceByValue<br>Deprecated version: N/A|Method or attribute name: traceByValue<br>Deprecated version: 8<br>New API: ohos.hiTraceMeter.traceByValue   |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: querySelfFaultLog<br>Deprecated version: N/A|Method or attribute name: querySelfFaultLog<br>Deprecated version: 9<br>New API: ohos.faultlogger/FaultLogger|@ohos.faultLogger.d.ts|
|Deprecated version changed|Method or attribute name: querySelfFaultLog<br>Deprecated version: N/A|Method or attribute name: querySelfFaultLog<br>Deprecated version: 9<br>New API: ohos.faultlogger/FaultLogger|@ohos.faultLogger.d.ts|
|Deprecated version changed|Class name: hiAppEvent<br>Deprecated version: N/A|Class name: hiAppEvent<br>Deprecated version: 9<br>New API: ohos.hiviewdfx.hiAppEvent |@ohos.hiAppEvent.d.ts|
|Deprecated version changed|Method or attribute name: write<br>Deprecated version: 9|Method or attribute name: write<br>Deprecated version: N/A<br>New API: ohos.hiviewdfx.hiAppEvent |@ohos.hiAppEvent.d.ts|
|Deprecated version changed|Method or attribute name: write<br>Deprecated version: 9|Method or attribute name: write<br>Deprecated version: N/A|@ohos.hiAppEvent.d.ts|
|Deprecated version changed|Method or attribute name: addRule<br>Deprecated version: N/A|Method or attribute name: addRule<br>Deprecated version: 9<br>New API: ohos.hichecker/hichecker|@ohos.hichecker.d.ts|
|Deprecated version changed|Method or attribute name: removeRule<br>Deprecated version: N/A|Method or attribute name: removeRule<br>Deprecated version: 9<br>New API: ohos.hichecker/hichecker|@ohos.hichecker.d.ts|
|Deprecated version changed|Method or attribute name: contains<br>Deprecated version: N/A|Method or attribute name: contains<br>Deprecated version: 9<br>New API: ohos.hichecker/hichecker|@ohos.hichecker.d.ts|
|Deprecated version changed|Method or attribute name: startProfiling<br>Deprecated version: N/A|Method or attribute name: startProfiling<br>Deprecated version: 9<br>New API: ohos.hidebug/hidebug.startJsCpuProfiling     |@ohos.hidebug.d.ts|
|Deprecated version changed|Method or attribute name: stopProfiling<br>Deprecated version: N/A|Method or attribute name: stopProfiling<br>Deprecated version: 9<br>New API: ohos.hidebug/hidebug.stopJsCpuProfiling     |@ohos.hidebug.d.ts|
|Deprecated version changed|Method or attribute name: dumpHeapData<br>Deprecated version: N/A|Method or attribute name: dumpHeapData<br>Deprecated version: 9<br>New API: ohos.hidebug/hidebug.dumpJsHeapData     |@ohos.hidebug.d.ts|
