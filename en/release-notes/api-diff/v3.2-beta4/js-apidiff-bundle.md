| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl<br>Method or attribute name: setDisposedStatus|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl<br>Method or attribute name: setDisposedStatus|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl<br>Method or attribute name: getDisposedStatus|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl<br>Method or attribute name: getDisposedStatus|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl<br>Method or attribute name: deleteDisposedStatus|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.appControl<br>Class name: appControl<br>Method or attribute name: deleteDisposedStatus|@ohos.bundle.appControl.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_DEFAULT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_APPLICATION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_HAP_MODULE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_ABILITY|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_EXTENSION_ABILITY|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_REQUESTED_PERMISSION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_METADATA|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_DISABLE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_INFO_WITH_SIGNATURE_INFO|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ApplicationFlag|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ApplicationFlag<br>Method or attribute name: GET_APPLICATION_INFO_DEFAULT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ApplicationFlag<br>Method or attribute name: GET_APPLICATION_INFO_WITH_PERMISSION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ApplicationFlag<br>Method or attribute name: GET_APPLICATION_INFO_WITH_METADATA|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ApplicationFlag<br>Method or attribute name: GET_APPLICATION_INFO_WITH_DISABLE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag<br>Method or attribute name: GET_ABILITY_INFO_DEFAULT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag<br>Method or attribute name: GET_ABILITY_INFO_WITH_PERMISSION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag<br>Method or attribute name: GET_ABILITY_INFO_WITH_APPLICATION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag<br>Method or attribute name: GET_ABILITY_INFO_WITH_METADATA|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag<br>Method or attribute name: GET_ABILITY_INFO_WITH_DISABLE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityFlag<br>Method or attribute name: GET_ABILITY_INFO_ONLY_SYSTEM_APP|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityFlag|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityFlag<br>Method or attribute name: GET_EXTENSION_ABILITY_INFO_DEFAULT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityFlag<br>Method or attribute name: GET_EXTENSION_ABILITY_INFO_WITH_PERMISSION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityFlag<br>Method or attribute name: GET_EXTENSION_ABILITY_INFO_WITH_APPLICATION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityFlag<br>Method or attribute name: GET_EXTENSION_ABILITY_INFO_WITH_METADATA|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: FORM|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: WORK_SCHEDULER|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: INPUT_METHOD|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: SERVICE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: ACCESSIBILITY|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: DATA_SHARE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: FILE_SHARE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: STATIC_SUBSCRIBER|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: WALLPAPER|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: BACKUP|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: WINDOW|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: ENTERPRISE_ADMIN|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: THUMBNAIL|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: PREVIEW|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: ExtensionAbilityType<br>Method or attribute name: UNSPECIFIED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: PermissionGrantState|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: PermissionGrantState<br>Method or attribute name: PERMISSION_DENIED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: PermissionGrantState<br>Method or attribute name: PERMISSION_GRANTED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: SupportWindowMode|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: SupportWindowMode<br>Method or attribute name: FULL_SCREEN|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: SupportWindowMode<br>Method or attribute name: SPLIT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: SupportWindowMode<br>Method or attribute name: FLOATING|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: LaunchType|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: LaunchType<br>Method or attribute name: SINGLETON|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: LaunchType<br>Method or attribute name: STANDARD|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: LaunchType<br>Method or attribute name: SPECIFIED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityType|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityType<br>Method or attribute name: PAGE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityType<br>Method or attribute name: SERVICE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: AbilityType<br>Method or attribute name: DATA|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: UNSPECIFIED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: LANDSCAPE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: PORTRAIT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: FOLLOW_RECENT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: LANDSCAPE_INVERTED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: PORTRAIT_INVERTED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_LANDSCAPE|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_PORTRAIT|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_RESTRICTED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_LANDSCAPE_RESTRICTED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_PORTRAIT_RESTRICTED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: DisplayOrientation<br>Method or attribute name: LOCKED|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfoForSelf|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfoForSelf|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getApplicationInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getApplicationInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getApplicationInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAllBundleInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAllBundleInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAllBundleInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAllApplicationInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAllApplicationInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAllApplicationInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: queryAbilityInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: queryAbilityInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: queryAbilityInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: queryExtensionAbilityInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: queryExtensionAbilityInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: queryExtensionAbilityInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleNameByUid|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleNameByUid|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleArchiveInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleArchiveInfo|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: cleanBundleCacheFiles|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: cleanBundleCacheFiles|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: setApplicationEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: setApplicationEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: setAbilityEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: setAbilityEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: isApplicationEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: isApplicationEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: isAbilityEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: isAbilityEnabled|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getLaunchWantForBundle|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getLaunchWantForBundle|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getLaunchWantForBundle|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getProfileByAbility|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getProfileByAbility|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getProfileByExtensionAbility|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getProfileByExtensionAbility|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getPermissionDef|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getPermissionDef|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAbilityLabel|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAbilityLabel|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAbilityIcon|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getAbilityIcon|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getApplicationInfoSync|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getApplicationInfoSync|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfoSync|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleManager<br>Class name: bundleManager<br>Method or attribute name: getBundleInfoSync|@ohos.bundle.bundleManager.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: BundleChangedInfo|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: BundleChangedInfo<br>Method or attribute name: bundleName|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: BundleChangedInfo<br>Method or attribute name: userId|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor<br>Method or attribute name: on_add|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor<br>Method or attribute name: on_update|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor<br>Method or attribute name: on_remove|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor<br>Method or attribute name: off_add|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor<br>Method or attribute name: off_update|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.bundleMonitor<br>Class name: bundleMonitor<br>Method or attribute name: off_remove|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: BROWSER<br>Function name: BROWSER = "Web Browser"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: IMAGE<br>Function name: IMAGE = "Image Gallery"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: AUDIO<br>Function name: AUDIO = "Audio Player"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: VIDEO<br>Function name: VIDEO = "Video Player"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: PDF<br>Function name: PDF = "PDF Viewer"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: WORD<br>Function name: WORD = "Word Viewer"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: EXCEL<br>Function name: EXCEL = "Excel Viewer"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Method or attribute name: PPT<br>Function name: PPT = "PPT Viewer"|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo|@ohos.bundle.distributedBundle.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: UpgradeFlag|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: UpgradeFlag<br>Method or attribute name: NOT_UPGRADE|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: UpgradeFlag<br>Method or attribute name: SINGLE_UPGRADE|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: UpgradeFlag<br>Method or attribute name: RELATION_UPGRADE|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: BundlePackFlag|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: BundlePackFlag<br>Method or attribute name: GET_PACK_INFO_ALL|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: BundlePackFlag<br>Method or attribute name: GET_PACKAGES|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: BundlePackFlag<br>Method or attribute name: GET_BUNDLE_SUMMARY|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: BundlePackFlag<br>Method or attribute name: GET_MODULE_SUMMARY|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: setHapModuleUpgradeFlag|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: setHapModuleUpgradeFlag|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: isHapModuleRemovable|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: isHapModuleRemovable|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: getBundlePackInfo|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: getBundlePackInfo|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: getDispatchInfo|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.freeInstall<br>Class name: freeInstall<br>Method or attribute name: getDispatchInfo|@ohos.bundle.freeInstall.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: installer|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: installer<br>Method or attribute name: getBundleInstaller|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: installer<br>Method or attribute name: getBundleInstaller|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: BundleInstaller|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: BundleInstaller<br>Method or attribute name: install|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: BundleInstaller<br>Method or attribute name: uninstall|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: BundleInstaller<br>Method or attribute name: recover|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: HashParam|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: HashParam<br>Method or attribute name: moduleName|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: HashParam<br>Method or attribute name: hashValue|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: InstallParam|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: InstallParam<br>Method or attribute name: userId|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: InstallParam<br>Method or attribute name: installFlag|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: InstallParam<br>Method or attribute name: isKeepData|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: InstallParam<br>Method or attribute name: hashParams|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.installer<br>Class name: InstallParam<br>Method or attribute name: crowdtestDeadline|@ohos.bundle.installer.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager<br>Method or attribute name: getLauncherAbilityInfo|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager<br>Method or attribute name: getLauncherAbilityInfo|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager<br>Method or attribute name: getAllLauncherAbilityInfo|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager<br>Method or attribute name: getAllLauncherAbilityInfo|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager<br>Method or attribute name: getShortcutInfo|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.bundle.launcherBundleManager<br>Class name: launcherBundleManager<br>Method or attribute name: getShortcutInfo|@ohos.bundle.launcherBundleManager.d.ts|
|Added||Module name: ohos.zlib<br>Class name: zlib<br>Method or attribute name: compressFile|@ohos.zlib.d.ts|
|Added||Module name: ohos.zlib<br>Class name: zlib<br>Method or attribute name: compressFile|@ohos.zlib.d.ts|
|Added||Module name: ohos.zlib<br>Class name: zlib<br>Method or attribute name: decompressFile|@ohos.zlib.d.ts|
|Added||Module name: ohos.zlib<br>Class name: zlib<br>Method or attribute name: decompressFile|@ohos.zlib.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: type|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: orientation|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: launchType|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: metadata|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: supportWindowModes|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: windowSize|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize<br>Method or attribute name: maxWindowRatio|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize<br>Method or attribute name: minWindowRatio|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize<br>Method or attribute name: maxWindowWidth|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize<br>Method or attribute name: minWindowWidth|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize<br>Method or attribute name: maxWindowHeight|abilityInfo.d.ts|
|Added||Module name: abilityInfo<br>Class name: WindowSize<br>Method or attribute name: minWindowHeight|abilityInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: labelId|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: iconId|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: metadata|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: iconResource|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: labelResource|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: descriptionResource|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: appDistributionType|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: appProvisionType|applicationInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: BundleInfo<br>Method or attribute name: hapModulesInfo|bundleInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: BundleInfo<br>Method or attribute name: permissionGrantStates|bundleInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: BundleInfo<br>Method or attribute name: signatureInfo|bundleInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: ReqPermissionDetail<br>Method or attribute name: reasonId|bundleInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: SignatureInfo|bundleInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: SignatureInfo<br>Method or attribute name: appId|bundleInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: SignatureInfo<br>Method or attribute name: fingerprint|bundleInfo.d.ts|
|Added||Module name: dispatchInfo<br>Class name: DispatchInfo|dispatchInfo.d.ts|
|Added||Module name: dispatchInfo<br>Class name: DispatchInfo<br>Method or attribute name: version|dispatchInfo.d.ts|
|Added||Module name: dispatchInfo<br>Class name: DispatchInfo<br>Method or attribute name: dispatchAPIVersion|dispatchInfo.d.ts|
|Added||Module name: elementName<br>Class name: ElementName<br>Method or attribute name: moduleName|elementName.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: bundleName|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: moduleName|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: name|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: labelId|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: descriptionId|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: iconId|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: isVisible|extensionAbilityInfo.d.ts|
|Added||Method or attribute name: extensionAbilityType<br>Function name: readonly extensionAbilityType: bundleManager.ExtensionAbilityType;|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: permissions|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: applicationInfo|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: metadata|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: enabled|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: readPermission|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: writePermission|extensionAbilityInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: mainElementName|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: abilitiesInfo|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: extensionAbilitiesInfo|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: metadata|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: hashValue|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: moduleSourceDir|hapModuleInfo.d.ts|
|Added||Module name: metadata<br>Class name: Metadata<br>Method or attribute name: name|metadata.d.ts|
|Added||Module name: metadata<br>Class name: Metadata<br>Method or attribute name: value|metadata.d.ts|
|Added||Module name: metadata<br>Class name: Metadata<br>Method or attribute name: resource|metadata.d.ts|
|Added||Module name: packInfo<br>Class name: BundlePackInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundlePackInfo<br>Method or attribute name: packages|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundlePackInfo<br>Method or attribute name: summary|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: deviceTypes|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: moduleType|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: deliveryWithInstall|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageSummary|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageSummary<br>Method or attribute name: app|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageSummary<br>Method or attribute name: modules|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundleConfigInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundleConfigInfo<br>Method or attribute name: bundleName|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundleConfigInfo<br>Method or attribute name: version|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ExtensionAbility|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ExtensionAbility<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ExtensionAbility<br>Method or attribute name: forms|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: mainAbility|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: apiVersion|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: deviceTypes|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: distro|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: abilities|packInfo.d.ts|
|Added||Method or attribute name: extensionAbilities<br>Function name: readonly extensionAbilities: Array<ExtensionAbility>;|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: deliveryWithInstall|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: installationFree|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: moduleName|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: moduleType|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: label|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: visible|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: forms|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: type|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: updateEnabled|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: scheduledUpdateTime|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: updateDuration|packInfo.d.ts|
|Added||Method or attribute name: supportDimensions<br>Function name: readonly supportDimensions: Array<string>;|packInfo.d.ts|
|Added||Method or attribute name: defaultDimension<br>Function name: readonly defaultDimension: string;|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version<br>Method or attribute name: minCompatibleVersionCode|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version<br>Method or attribute name: code|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion<br>Method or attribute name: releaseType|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion<br>Method or attribute name: compatible|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion<br>Method or attribute name: target|packInfo.d.ts|
|Added||Module name: permissionDef<br>Class name: PermissionDef|permissionDef.d.ts|
|Added||Module name: permissionDef<br>Class name: PermissionDef<br>Method or attribute name: permissionName|permissionDef.d.ts|
|Added||Module name: permissionDef<br>Class name: PermissionDef<br>Method or attribute name: grantMode|permissionDef.d.ts|
|Added||Module name: permissionDef<br>Class name: PermissionDef<br>Method or attribute name: labelId|permissionDef.d.ts|
|Added||Module name: permissionDef<br>Class name: PermissionDef<br>Method or attribute name: descriptionId|permissionDef.d.ts|
|Added||Method or attribute name: moduleName<br>Function name: readonly moduleName: string;|shortcutInfo.d.ts|
|Added||Module name: shortcutInfo<br>Class name: ShortcutWant<br>Method or attribute name: targetModule|shortcutInfo.d.ts|
|Added||Module name: shortcutInfo<br>Class name: ShortcutWant<br>Method or attribute name: targetAbility|shortcutInfo.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_WITH_EXTENSION_ABILITY||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_WITH_HASH_VALUE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: BundleFlag<br>Method or attribute name: GET_APPLICATION_INFO_WITH_CERTIFICATE_FINGERPRINT||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionFlag||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_DEFAULT||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_WITH_PERMISSION||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_WITH_APPLICATION||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_WITH_METADATA||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: LANDSCAPE_INVERTED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: PORTRAIT_INVERTED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_LANDSCAPE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_PORTRAIT||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_RESTRICTED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_LANDSCAPE_RESTRICTED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_PORTRAIT_RESTRICTED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: LOCKED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: FORM||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: WORK_SCHEDULER||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: INPUT_METHOD||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: SERVICE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: ACCESSIBILITY||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: DATA_SHARE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: FILE_SHARE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: STATIC_SUBSCRIBER||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: WALLPAPER||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: BACKUP||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: WINDOW||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: ENTERPRISE_ADMIN||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: THUMBNAIL||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: PREVIEW||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: UNSPECIFIED||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: UpgradeFlag||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: UpgradeFlag<br>Method or attribute name: NOT_UPGRADE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: UpgradeFlag<br>Method or attribute name: SINGLE_UPGRADE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: UpgradeFlag<br>Method or attribute name: RELATION_UPGRADE||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: SupportWindowMode||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: SupportWindowMode<br>Method or attribute name: FULL_SCREEN||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: SupportWindowMode<br>Method or attribute name: SPLIT||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: SupportWindowMode<br>Method or attribute name: FLOATING||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: queryExtensionAbilityInfos||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: queryExtensionAbilityInfos||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: queryExtensionAbilityInfos||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setModuleUpgradeFlag||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setModuleUpgradeFlag||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: isModuleRemovable||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: isModuleRemovable||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundlePackInfo||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundlePackInfo||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDispatcherVersion||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDispatcherVersion||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByAbility||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByAbility||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByExtensionAbility||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByExtensionAbility||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setDisposedStatus||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setDisposedStatus||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDisposedStatus||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDisposedStatus||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getApplicationInfoSync||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getApplicationInfoSync||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundleInfoSync||@ohos.bundle.d.ts|
|Deleted||Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundleInfoSync||@ohos.bundle.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: supportWindowMode||abilityInfo.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: maxWindowRatio||abilityInfo.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: minWindowRatio||abilityInfo.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: maxWindowWidth||abilityInfo.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: minWindowWidth||abilityInfo.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: maxWindowHeight||abilityInfo.d.ts|
|Deleted||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: minWindowHeight||abilityInfo.d.ts|
|Deleted||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: labelIndex||applicationInfo.d.ts|
|Deleted||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: iconIndex||applicationInfo.d.ts|
|Deleted||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: fingerprint||applicationInfo.d.ts|
|Deleted||Module name: bundleInfo<br>Class name: BundleInfo<br>Method or attribute name: extensionAbilityInfo||bundleInfo.d.ts|
|Deleted||Module name: bundleInstaller<br>Class name: HashParam||bundleInstaller.d.ts|
|Deleted||Module name: bundleInstaller<br>Class name: HashParam<br>Method or attribute name: moduleName||bundleInstaller.d.ts|
|Deleted||Module name: bundleInstaller<br>Class name: HashParam<br>Method or attribute name: hashValue||bundleInstaller.d.ts|
|Deleted||Module name: bundleInstaller<br>Class name: InstallParam<br>Method or attribute name: hashParams||bundleInstaller.d.ts|
|Deleted||Module name: bundleInstaller<br>Class name: InstallParam<br>Method or attribute name: crowdtestDeadline||bundleInstaller.d.ts|
|Deleted||Module name: dispatchInfo<br>Class name: DispatchInfo<br>Method or attribute name: dispatchAPI||dispatchInfo.d.ts|
|Deleted||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: extensionAbilityInfo||hapModuleInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: deviceType||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: ExtensionAbilities||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: ExtensionAbilities<br>Method or attribute name: name||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: ExtensionAbilities<br>Method or attribute name: forms||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: deviceType||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: mainAbility||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: BundlePackFlag||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_PACK_INFO_ALL||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_PACKAGES||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_BUNDLE_SUMMARY||packInfo.d.ts|
|Deleted||Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_MODULE_SUMMARY||packInfo.d.ts|
|Deprecated version changed|Class name: bundle<br>Deprecated version: N/A|Class name: bundle<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: BundleFlag<br>Deprecated version: N/A|Class name: BundleFlag<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.BundleFlag|@ohos.bundle.d.ts|
|Deprecated version changed|Class name: ColorMode<br>Deprecated version: N/A|Class name: ColorMode<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: GrantStatus<br>Deprecated version: N/A|Class name: GrantStatus<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.PermissionGrantState |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: AbilityType<br>Deprecated version: N/A|Class name: AbilityType<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.AbilityType   |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: AbilitySubType<br>Deprecated version: N/A|Class name: AbilitySubType<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: DisplayOrientation<br>Deprecated version: N/A|Class name: DisplayOrientation<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.DisplayOrientation   |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: LaunchMode<br>Deprecated version: N/A|Class name: LaunchMode<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.LaunchType   |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: BundleOptions<br>Deprecated version: N/A|Class name: BundleOptions<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager |@ohos.bundle.d.ts|
|Deprecated version changed|Class name: InstallErrorCode<br>Deprecated version: N/A|Class name: InstallErrorCode<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager |@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleInfo<br>Deprecated version: N/A|Method or attribute name: getBundleInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleInfo<br>Deprecated version: N/A|Method or attribute name: getBundleInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleInfo<br>Deprecated version: N/A|Method or attribute name: getBundleInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleInstaller<br>Deprecated version: N/A|Method or attribute name: getBundleInstaller<br>Deprecated version: 9<br>New API: ohos.bundle.installer|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleInstaller<br>Deprecated version: N/A|Method or attribute name: getBundleInstaller<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAbilityInfo<br>Deprecated version: N/A|Method or attribute name: getAbilityInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAbilityInfo<br>Deprecated version: N/A|Method or attribute name: getAbilityInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getApplicationInfo<br>Deprecated version: N/A|Method or attribute name: getApplicationInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getApplicationInfo<br>Deprecated version: N/A|Method or attribute name: getApplicationInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getApplicationInfo<br>Deprecated version: N/A|Method or attribute name: getApplicationInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: queryAbilityByWant<br>Deprecated version: N/A|Method or attribute name: queryAbilityByWant<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: queryAbilityByWant<br>Deprecated version: N/A|Method or attribute name: queryAbilityByWant<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: queryAbilityByWant<br>Deprecated version: N/A|Method or attribute name: queryAbilityByWant<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAllBundleInfo<br>Deprecated version: N/A|Method or attribute name: getAllBundleInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAllBundleInfo<br>Deprecated version: N/A|Method or attribute name: getAllBundleInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAllBundleInfo<br>Deprecated version: N/A|Method or attribute name: getAllBundleInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAllApplicationInfo<br>Deprecated version: N/A|Method or attribute name: getAllApplicationInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAllApplicationInfo<br>Deprecated version: N/A|Method or attribute name: getAllApplicationInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAllApplicationInfo<br>Deprecated version: N/A|Method or attribute name: getAllApplicationInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getNameForUid<br>Deprecated version: N/A|Method or attribute name: getNameForUid<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getNameForUid<br>Deprecated version: N/A|Method or attribute name: getNameForUid<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleArchiveInfo<br>Deprecated version: N/A|Method or attribute name: getBundleArchiveInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getBundleArchiveInfo<br>Deprecated version: N/A|Method or attribute name: getBundleArchiveInfo<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getLaunchWantForBundle<br>Deprecated version: N/A|Method or attribute name: getLaunchWantForBundle<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getLaunchWantForBundle<br>Deprecated version: N/A|Method or attribute name: getLaunchWantForBundle<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: cleanBundleCacheFiles<br>Deprecated version: N/A|Method or attribute name: cleanBundleCacheFiles<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: cleanBundleCacheFiles<br>Deprecated version: N/A|Method or attribute name: cleanBundleCacheFiles<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: setApplicationEnabled<br>Deprecated version: N/A|Method or attribute name: setApplicationEnabled<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: setApplicationEnabled<br>Deprecated version: N/A|Method or attribute name: setApplicationEnabled<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: setAbilityEnabled<br>Deprecated version: N/A|Method or attribute name: setAbilityEnabled<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: setAbilityEnabled<br>Deprecated version: N/A|Method or attribute name: setAbilityEnabled<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getPermissionDef<br>Deprecated version: N/A|Method or attribute name: getPermissionDef<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getPermissionDef<br>Deprecated version: N/A|Method or attribute name: getPermissionDef<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAbilityLabel<br>Deprecated version: N/A|Method or attribute name: getAbilityLabel<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAbilityLabel<br>Deprecated version: N/A|Method or attribute name: getAbilityLabel<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAbilityIcon<br>Deprecated version: N/A|Method or attribute name: getAbilityIcon<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: getAbilityIcon<br>Deprecated version: N/A|Method or attribute name: getAbilityIcon<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: isAbilityEnabled<br>Deprecated version: N/A|Method or attribute name: isAbilityEnabled<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: isAbilityEnabled<br>Deprecated version: N/A|Method or attribute name: isAbilityEnabled<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: isApplicationEnabled<br>Deprecated version: N/A|Method or attribute name: isApplicationEnabled<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager|@ohos.bundle.d.ts|
|Deprecated version changed|Method or attribute name: isApplicationEnabled<br>Deprecated version: N/A|Method or attribute name: isApplicationEnabled<br>Deprecated version: 9|@ohos.bundle.d.ts|
|Deprecated version changed|Class name: innerBundleManager<br>Deprecated version: N/A|Class name: innerBundleManager<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager |@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: getLauncherAbilityInfos<br>Deprecated version: N/A|Method or attribute name: getLauncherAbilityInfos<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: getLauncherAbilityInfos<br>Deprecated version: N/A|Method or attribute name: getLauncherAbilityInfos<br>Deprecated version: 9|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: on_BundleStatusChange<br>Deprecated version: N/A|Method or attribute name: on_BundleStatusChange<br>Deprecated version: 9<br>New API: ohos.bundle.bundleMonitor|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: on_BundleStatusChange<br>Deprecated version: N/A|Method or attribute name: on_BundleStatusChange<br>Deprecated version: 9|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: off_BundleStatusChange<br>Deprecated version: N/A|Method or attribute name: off_BundleStatusChange<br>Deprecated version: 9<br>New API: ohos.bundle.bundleMonitor|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: off_BundleStatusChange<br>Deprecated version: N/A|Method or attribute name: off_BundleStatusChange<br>Deprecated version: 9|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: getAllLauncherAbilityInfos<br>Deprecated version: N/A|Method or attribute name: getAllLauncherAbilityInfos<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: getAllLauncherAbilityInfos<br>Deprecated version: N/A|Method or attribute name: getAllLauncherAbilityInfos<br>Deprecated version: 9|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: getShortcutInfos<br>Deprecated version: N/A|Method or attribute name: getShortcutInfos<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Method or attribute name: getShortcutInfos<br>Deprecated version: N/A|Method or attribute name: getShortcutInfos<br>Deprecated version: 9|@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Class name: distributedBundle<br>Deprecated version: N/A|Class name: distributedBundle<br>Deprecated version: 9<br>New API: ohos.bundle.distributeBundle |@ohos.distributedBundle.d.ts|
|Deprecated version changed|Method or attribute name: getRemoteAbilityInfo<br>Deprecated version: N/A|Method or attribute name: getRemoteAbilityInfo<br>Deprecated version: 9<br>New API: ohos.bundle.distributeBundle|@ohos.distributedBundle.d.ts|
|Deprecated version changed|Method or attribute name: getRemoteAbilityInfo<br>Deprecated version: N/A|Method or attribute name: getRemoteAbilityInfo<br>Deprecated version: 9|@ohos.distributedBundle.d.ts|
|Deprecated version changed|Method or attribute name: getRemoteAbilityInfos<br>Deprecated version: N/A|Method or attribute name: getRemoteAbilityInfos<br>Deprecated version: 9<br>New API: ohos.bundle.distributeBundle|@ohos.distributedBundle.d.ts|
|Deprecated version changed|Method or attribute name: getRemoteAbilityInfos<br>Deprecated version: N/A|Method or attribute name: getRemoteAbilityInfos<br>Deprecated version: 9|@ohos.distributedBundle.d.ts|
|Deprecated version changed|Class name: ErrorCode<br>Deprecated version: N/A|Class name: ErrorCode<br>Deprecated version: 9|@ohos.zlib.d.ts|
|Deprecated version changed|Method or attribute name: zipFile<br>Deprecated version: N/A|Method or attribute name: zipFile<br>Deprecated version: 9<br>New API: ohos.zlib|@ohos.zlib.d.ts|
|Deprecated version changed|Method or attribute name: unzipFile<br>Deprecated version: N/A|Method or attribute name: unzipFile<br>Deprecated version: 9<br>New API: ohos.zlib|@ohos.zlib.d.ts|
|Deprecated version changed|Class name: CheckPackageHasInstalledResponse<br>Deprecated version: N/A|Class name: CheckPackageHasInstalledResponse<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Class name: CheckPackageHasInstalledOptions<br>Deprecated version: N/A|Class name: CheckPackageHasInstalledOptions<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Class name: Package<br>Deprecated version: N/A|Class name: Package<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Method or attribute name: hasInstalled<br>Deprecated version: N/A|Method or attribute name: hasInstalled<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Class name: AbilityInfo<br>Deprecated version: N/A|Class name: AbilityInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.AbilityInfo |abilityInfo.d.ts|
|Deprecated version changed|Class name: ApplicationInfo<br>Deprecated version: N/A|Class name: ApplicationInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.ApplicationInfo |applicationInfo.d.ts|
|Deprecated version changed|Class name: UsedScene<br>Deprecated version: N/A|Class name: UsedScene<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.UsedScene |bundleInfo.d.ts|
|Deprecated version changed|Class name: ReqPermissionDetail<br>Deprecated version: N/A|Class name: ReqPermissionDetail<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.ReqPermissionDetail |bundleInfo.d.ts|
|Deprecated version changed|Class name: BundleInfo<br>Deprecated version: N/A|Class name: BundleInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.BundleInfo |bundleInfo.d.ts|
|Deprecated version changed|Class name: InstallParam<br>Deprecated version: N/A|Class name: InstallParam<br>Deprecated version: 9<br>New API: ohos.bundle.installer|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: userId<br>Deprecated version: N/A|Method or attribute name: userId<br>Deprecated version: 9<br>New API: ohos.bundle.installer.InstallParam|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: installFlag<br>Deprecated version: N/A|Method or attribute name: installFlag<br>Deprecated version: 9<br>New API: ohos.bundle.installer.InstallParam|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: isKeepData<br>Deprecated version: N/A|Method or attribute name: isKeepData<br>Deprecated version: 9<br>New API: ohos.bundle.installer.InstallParam|bundleInstaller.d.ts|
|Deprecated version changed|Class name: InstallStatus<br>Deprecated version: N/A|Class name: InstallStatus<br>Deprecated version: 9|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: status<br>Deprecated version: N/A|Method or attribute name: status<br>Deprecated version: 9|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: statusMessage<br>Deprecated version: N/A|Method or attribute name: statusMessage<br>Deprecated version: 9|bundleInstaller.d.ts|
|Deprecated version changed|Class name: BundleInstaller<br>Deprecated version: N/A|Class name: BundleInstaller<br>Deprecated version: 9<br>New API: ohos.bundle.installer|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: install<br>Deprecated version: N/A|Method or attribute name: install<br>Deprecated version: 9<br>New API: ohos.bundle.installer.BundleInstaller|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: uninstall<br>Deprecated version: N/A|Method or attribute name: uninstall<br>Deprecated version: 9<br>New API: ohos.bundle.installer.BundleInstaller|bundleInstaller.d.ts|
|Deprecated version changed|Method or attribute name: recover<br>Deprecated version: N/A|Method or attribute name: recover<br>Deprecated version: 9<br>New API: ohos.bundle.installer.BundleInstaller|bundleInstaller.d.ts|
|Deprecated version changed|Class name: BundleStatusCallback<br>Deprecated version: N/A|Class name: BundleStatusCallback<br>Deprecated version: 9|bundleStatusCallback.d.ts|
|Deprecated version changed|Class name: CustomizeData<br>Deprecated version: N/A|Class name: CustomizeData<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.Metadata |customizeData.d.ts|
|Deprecated version changed|Class name: ElementName<br>Deprecated version: N/A|Class name: ElementName<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.ElementName |elementName.d.ts|
|Deprecated version changed|Class name: HapModuleInfo<br>Deprecated version: N/A|Class name: HapModuleInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.HapModuleInfo |hapModuleInfo.d.ts|
|Deprecated version changed|Class name: LauncherAbilityInfo<br>Deprecated version: N/A|Class name: LauncherAbilityInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.LauncherAbilityInfo |launcherAbilityInfo.d.ts|
|Deprecated version changed|Class name: ModuleInfo<br>Deprecated version: N/A|Class name: ModuleInfo<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.HapModuleInfo |moduleInfo.d.ts|
|Deprecated version changed|Class name: PermissionDef<br>Deprecated version: N/A|Class name: PermissionDef<br>Deprecated version: 9<br>New API: ohos.bundle.bundleManager.PermissionDef |PermissionDef.d.ts|
|Deprecated version changed|Class name: RemoteAbilityInfo<br>Deprecated version: N/A|Class name: RemoteAbilityInfo<br>Deprecated version: 9<br>New API: ohos.bundle.distributedBundle.RemoteAbilityInfo |remoteAbilityInfo.d.ts|
|Deprecated version changed|Class name: ShortcutWant<br>Deprecated version: N/A|Class name: ShortcutWant<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager.ShortcutWant  |shortcutInfo.d.ts|
|Deprecated version changed|Class name: ShortcutInfo<br>Deprecated version: N/A|Class name: ShortcutInfo<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager.ShortcutInfo    |shortcutInfo.d.ts|
|Error code added||Method or attribute name: isDefaultApplication<br>Error code: 401, 801|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: isDefaultApplication<br>Error code: 401, 801|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: getDefaultApplication<br>Error code: 201, 401,  801, 17700004, 17700023, 17700025|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: getDefaultApplication<br>Error code: 201, 401,  801, 17700004, 17700023, 17700025|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: setDefaultApplication<br>Error code: 201, 401, 801, 17700004, 17700025, 17700028|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: setDefaultApplication<br>Error code: 201, 401, 801, 17700004, 17700025, 17700028|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: resetDefaultApplication<br>Error code: 201, 401, 801, 17700004, 17700025|@ohos.bundle.defaultAppManager.d.ts|
|Error code added||Method or attribute name: resetDefaultApplication<br>Error code: 201, 401, 801, 17700004, 17700025|@ohos.bundle.defaultAppManager.d.ts|
