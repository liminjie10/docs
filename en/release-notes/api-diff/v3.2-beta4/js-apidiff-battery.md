| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.power<br>Class name: power<br>Method or attribute name: shutdown|@ohos.power.d.ts|
|Added||Module name: ohos.power<br>Class name: power<br>Method or attribute name: reboot|@ohos.power.d.ts|
|Added||Module name: ohos.power<br>Class name: power<br>Method or attribute name: isActive|@ohos.power.d.ts|
|Added||Module name: ohos.power<br>Class name: power<br>Method or attribute name: wakeup|@ohos.power.d.ts|
|Added||Module name: ohos.power<br>Class name: power<br>Method or attribute name: suspend|@ohos.power.d.ts|
|Added||Method or attribute name: getPowerMode<br>Function name: function getPowerMode(): DevicePowerMode;|@ohos.power.d.ts|
|Added||Module name: ohos.runningLock<br>Class name: RunningLock<br>Method or attribute name: hold|@ohos.runningLock.d.ts|
|Added||Module name: ohos.runningLock<br>Class name: RunningLock<br>Method or attribute name: isHolding|@ohos.runningLock.d.ts|
|Added||Module name: ohos.runningLock<br>Class name: RunningLock<br>Method or attribute name: unhold|@ohos.runningLock.d.ts|
|Added||Module name: ohos.runningLock<br>Class name: runningLock<br>Method or attribute name: isSupported|@ohos.runningLock.d.ts|
|Added||Module name: ohos.runningLock<br>Class name: runningLock<br>Method or attribute name: create|@ohos.runningLock.d.ts|
|Added||Module name: ohos.runningLock<br>Class name: runningLock<br>Method or attribute name: create|@ohos.runningLock.d.ts|
|Added||Module name: ohos.thermal<br>Class name: thermal<br>Method or attribute name: registerThermalLevelCallback|@ohos.thermal.d.ts|
|Added||Module name: ohos.thermal<br>Class name: thermal<br>Method or attribute name: unregisterThermalLevelCallback|@ohos.thermal.d.ts|
|Added||Module name: ohos.thermal<br>Class name: thermal<br>Method or attribute name: getLevel|@ohos.thermal.d.ts|
|Deleted|Module name: ohos.power<br>Class name: power<br>Method or attribute name: shutdownDevice||@ohos.power.d.ts|
|Deleted|Module name: ohos.power<br>Class name: power<br>Method or attribute name: wakeupDevice||@ohos.power.d.ts|
|Deleted|Module name: ohos.power<br>Class name: power<br>Method or attribute name: suspendDevice||@ohos.power.d.ts|
|Deprecated version changed|Method or attribute name: rebootDevice<br>Deprecated version: N/A|Method or attribute name: rebootDevice<br>Deprecated version: 9<br>New API: {@link power|@ohos.power.d.ts|
|Deprecated version changed|Method or attribute name: isScreenOn<br>Deprecated version: N/A|Method or attribute name: isScreenOn<br>Deprecated version: 9<br>New API: {@link power|@ohos.power.d.ts|
|Deprecated version changed|Method or attribute name: isScreenOn<br>Deprecated version: N/A|Method or attribute name: isScreenOn<br>Deprecated version: 9|@ohos.power.d.ts|
|Deprecated version changed|Method or attribute name: lock<br>Deprecated version: N/A|Method or attribute name: lock<br>Deprecated version: 9<br>New API: {@link RunningLock|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: isUsed<br>Deprecated version: N/A|Method or attribute name: isUsed<br>Deprecated version: 9<br>New API: {@link RunningLock|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: unlock<br>Deprecated version: N/A|Method or attribute name: unlock<br>Deprecated version: 9<br>New API: {@link RunningLock|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: isRunningLockTypeSupported<br>Deprecated version: N/A|Method or attribute name: isRunningLockTypeSupported<br>Deprecated version: 9<br>New API: {@link RunningLock|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: isRunningLockTypeSupported<br>Deprecated version: N/A|Method or attribute name: isRunningLockTypeSupported<br>Deprecated version: 9|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: createRunningLock<br>Deprecated version: N/A|Method or attribute name: createRunningLock<br>Deprecated version: 9<br>New API: {@link RunningLock|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: createRunningLock<br>Deprecated version: N/A|Method or attribute name: createRunningLock<br>Deprecated version: 9|@ohos.runningLock.d.ts|
|Deprecated version changed|Method or attribute name: subscribeThermalLevel<br>Deprecated version: N/A|Method or attribute name: subscribeThermalLevel<br>Deprecated version: 9<br>New API: {@link thermal|@ohos.thermal.d.ts|
|Deprecated version changed|Method or attribute name: unsubscribeThermalLevel<br>Deprecated version: N/A|Method or attribute name: unsubscribeThermalLevel<br>Deprecated version: 9<br>New API: {@link thermal|@ohos.thermal.d.ts|
|Deprecated version changed|Method or attribute name: getThermalLevel<br>Deprecated version: N/A|Method or attribute name: getThermalLevel<br>Deprecated version: 9<br>New API: {@link thermal|@ohos.thermal.d.ts|
|Deprecated version changed|Class name: BatteryResponse<br>Deprecated version: 9|Class name: BatteryResponse<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Method or attribute name: charging<br>Deprecated version: 9|Method or attribute name: charging<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Method or attribute name: level<br>Deprecated version: 9|Method or attribute name: level<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Class name: GetStatusOptions<br>Deprecated version: 9|Class name: GetStatusOptions<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Method or attribute name: success<br>Deprecated version: 9|Method or attribute name: success<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Method or attribute name: fail<br>Deprecated version: 9|Method or attribute name: fail<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Method or attribute name: complete<br>Deprecated version: 9|Method or attribute name: complete<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Class name: Battery<br>Deprecated version: 9|Class name: Battery<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Method or attribute name: getStatus<br>Deprecated version: 9|Method or attribute name: getStatus<br>Deprecated version: 6|@system.battery.d.ts|
|Deprecated version changed|Class name: BrightnessResponse<br>Deprecated version: 9|Class name: BrightnessResponse<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: value<br>Deprecated version: 9|Method or attribute name: value<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: GetBrightnessOptions<br>Deprecated version: 9|Class name: GetBrightnessOptions<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: success<br>Deprecated version: 9|Method or attribute name: success<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: fail<br>Deprecated version: 9|Method or attribute name: fail<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: complete<br>Deprecated version: 9|Method or attribute name: complete<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: SetBrightnessOptions<br>Deprecated version: 9|Class name: SetBrightnessOptions<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: value<br>Deprecated version: 9|Method or attribute name: value<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: success<br>Deprecated version: 9|Method or attribute name: success<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: fail<br>Deprecated version: 9|Method or attribute name: fail<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: complete<br>Deprecated version: 9|Method or attribute name: complete<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: BrightnessModeResponse<br>Deprecated version: 9|Class name: BrightnessModeResponse<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: mode<br>Deprecated version: 9|Method or attribute name: mode<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: GetBrightnessModeOptions<br>Deprecated version: 9|Class name: GetBrightnessModeOptions<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: success<br>Deprecated version: 9|Method or attribute name: success<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: fail<br>Deprecated version: 9|Method or attribute name: fail<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: complete<br>Deprecated version: 9|Method or attribute name: complete<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: SetBrightnessModeOptions<br>Deprecated version: 9|Class name: SetBrightnessModeOptions<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: mode<br>Deprecated version: 9|Method or attribute name: mode<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: success<br>Deprecated version: 9|Method or attribute name: success<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: fail<br>Deprecated version: 9|Method or attribute name: fail<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: complete<br>Deprecated version: 9|Method or attribute name: complete<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: SetKeepScreenOnOptions<br>Deprecated version: 9|Class name: SetKeepScreenOnOptions<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: keepScreenOn<br>Deprecated version: 9|Method or attribute name: keepScreenOn<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: success<br>Deprecated version: 9|Method or attribute name: success<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: fail<br>Deprecated version: 9|Method or attribute name: fail<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: complete<br>Deprecated version: 9|Method or attribute name: complete<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Class name: Brightness<br>Deprecated version: 9|Class name: Brightness<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: getValue<br>Deprecated version: 9|Method or attribute name: getValue<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: setValue<br>Deprecated version: 9|Method or attribute name: setValue<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: getMode<br>Deprecated version: 9|Method or attribute name: getMode<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: setMode<br>Deprecated version: 9|Method or attribute name: setMode<br>Deprecated version: 7|@system.brightness.d.ts|
|Deprecated version changed|Method or attribute name: setKeepScreenOn<br>Deprecated version: 9|Method or attribute name: setKeepScreenOn<br>Deprecated version: 7|@system.brightness.d.ts|
