| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.notificationSubscribe<br>Class name: BundleOption|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: BundleOption<br>Method or attribute name: bundle|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: BundleOption<br>Method or attribute name: uid|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: NotificationKey|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: NotificationKey<br>Method or attribute name: id|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: NotificationKey<br>Method or attribute name: label|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: RemoveReason|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: RemoveReason<br>Method or attribute name: CLICK_REASON_REMOVE|@ohos.notificationSubscribe.d.ts|
|Added||Module name: ohos.notificationSubscribe<br>Class name: RemoveReason<br>Method or attribute name: CANCEL_REASON_REMOVE|@ohos.notificationSubscribe.d.ts|
|Deleted|Module name: ohos.notificationManager<br>Class name: NotificationKey||@ohos.notificationManager.d.ts|
|Deleted|Module name: ohos.notificationManager<br>Class name: NotificationKey<br>Method or attribute name: id||@ohos.notificationManager.d.ts|
|Deleted|Module name: ohos.notificationManager<br>Class name: NotificationKey<br>Method or attribute name: label||@ohos.notificationManager.d.ts|
|Deleted|Module name: ohos.notificationManager<br>Class name: RemoveReason||@ohos.notificationManager.d.ts|
|Deleted|Module name: ohos.notificationManager<br>Class name: RemoveReason<br>Method or attribute name: CLICK_REASON_REMOVE||@ohos.notificationManager.d.ts|
|Deleted|Module name: ohos.notificationManager<br>Class name: RemoveReason<br>Method or attribute name: CANCEL_REASON_REMOVE||@ohos.notificationManager.d.ts|
|Access level changed|Class name: notificationSubscribe<br>Access level: public API|Class name: notificationSubscribe<br>Access level: system API|@ohos.notificationSubscribe.d.ts|
|Access level changed|Class name: notificationSubscribe<br>Access level: public API|Class name: notificationSubscribe<br>Access level: system API|@ohos.notificationSubscribe.d.ts|
