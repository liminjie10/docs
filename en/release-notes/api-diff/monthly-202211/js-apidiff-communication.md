| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.net.connection<br>Class name: connection<br>Method or attribute name: isDefaultNetMetered|@ohos.net.connection.d.ts|
|Added||Module name: ohos.net.connection<br>Class name: connection<br>Method or attribute name: isDefaultNetMetered|@ohos.net.connection.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: bind|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: bind|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: getRemoteAddress|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: getRemoteAddress|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: getState|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: getState|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: setExtraOptions|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: setExtraOptions|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: on_message|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: off_message|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: on_connect|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: on_close|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: off_connect|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: off_close|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: on_error|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: off_error|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: close|@ohos.net.socket.d.ts|
|Added||Module name: ohos.net.socket<br>Class name: TLSSocket<br>Method or attribute name: close|@ohos.net.socket.d.ts|
|Added||Method or attribute name: cert<br>Function name: cert?: string;|@ohos.net.socket.d.ts|
|Added||Method or attribute name: key<br>Function name: key?: string;|@ohos.net.socket.d.ts|
|Added||Method or attribute name: NDEF_FORMATABLE<br>Function name: const NDEF_FORMATABLE = 7;|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: makeUriRecord|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: makeTextRecord|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: makeMimeRecord|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: makeExternalRecord|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: createNdefMessage|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: createNdefMessage|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.nfc.tag<br>Class name: ndef<br>Method or attribute name: messageToBytes|@ohos.nfc.tag.d.ts|
|Added||Module name: ohos.rpc<br>Class name: IRemoteObject<br>Method or attribute name: getDescriptor|@ohos.rpc.d.ts|
|Added||Method or attribute name: scan<br>Function name: function scan(): void;|@ohos.wifiManager.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefMessage<br>Method or attribute name: makeUriRecord||nfctech.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefMessage<br>Method or attribute name: makeTextRecord||nfctech.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefMessage<br>Method or attribute name: makeMimeRecord||nfctech.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefMessage<br>Method or attribute name: makeExternalRecord||nfctech.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefMessage<br>Method or attribute name: messageToBytes||nfctech.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefTag<br>Method or attribute name: createNdefMessage||nfctech.d.ts|
|Deleted|Module name: nfctech<br>Class name: NdefTag<br>Method or attribute name: createNdefMessage||nfctech.d.ts|
|Access level changed|Class name: WifiInfoElem<br>Access level: system API|Class name: WifiInfoElem<br>Access level: public API|@ohos.wifiManager.d.ts|
|Access level changed|Method or attribute name: eid<br>Access level: system API|Method or attribute name: eid<br>Access level: public API|@ohos.wifiManager.d.ts|
|Access level changed|Method or attribute name: content<br>Access level: system API|Method or attribute name: content<br>Access level: public API|@ohos.wifiManager.d.ts|
|Permission deleted|Method or attribute name: connect<br>Permission: ohos.permission.INTERNET|Method or attribute name: connect<br>Permission: N/A|@ohos.net.socket.d.ts|
|Permission deleted|Method or attribute name: connect<br>Permission: ohos.permission.INTERNET|Method or attribute name: connect<br>Permission: N/A|@ohos.net.socket.d.ts|
|Permission deleted|Method or attribute name: send<br>Permission: ohos.permission.INTERNET|Method or attribute name: send<br>Permission: N/A|@ohos.net.socket.d.ts|
|Permission deleted|Method or attribute name: send<br>Permission: ohos.permission.INTERNET|Method or attribute name: send<br>Permission: N/A|@ohos.net.socket.d.ts|
|Access level changed|Class name: WifiInfoElem<br>Access level: system API|Class name: WifiInfoElem<br>Access level: public API|@ohos.wifiManager.d.ts|
|Access level changed|Method or attribute name: eid<br>Access level: system API|Method or attribute name: eid<br>Access level: public API|@ohos.wifiManager.d.ts|
|Access level changed|Method or attribute name: content<br>Access level: system API|Method or attribute name: content<br>Access level: public API|@ohos.wifiManager.d.ts|
