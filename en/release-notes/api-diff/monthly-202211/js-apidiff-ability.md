| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: ACTION_APP_ACCOUNT_AUTH<br>Function name: ACTION_APP_ACCOUNT_AUTH = "ohos.appAccount.action.auth"|@ohos.ability.wantConstant.d.ts|
|Added||Module name: ohos.app.ability.appRecovery<br>Class name: appRecovery|@ohos.app.ability.appRecovery.d.ts|
|Added||Module name: ohos.app.ability.appRecovery<br>Class name: appRecovery<br>Method or attribute name: enableAppRecovery|@ohos.app.ability.appRecovery.d.ts|
|Added||Module name: ohos.app.ability.appRecovery<br>Class name: appRecovery<br>Method or attribute name: restartApp|@ohos.app.ability.appRecovery.d.ts|
|Added||Module name: ohos.app.ability.appRecovery<br>Class name: appRecovery<br>Method or attribute name: saveAppState|@ohos.app.ability.appRecovery.d.ts|
|Added||Module name: ohos.app.ability.UIAbility<br>Class name: UIAbility<br>Method or attribute name: onSaveState|@ohos.app.ability.UIAbility.d.ts|
|Deleted|Module name: ohos.app.ability.Ability<br>Class name: Ability<br>Method or attribute name: onSaveState||@ohos.app.ability.Ability.d.ts|
|Deleted|Module name: ohos.app.ability.appRecovery<br>Class name: appReceovery||@ohos.app.ability.appRecovery.d.ts|
|Deleted|Module name: ohos.app.ability.appRecovery<br>Class name: appReceovery<br>Method or attribute name: enableAppRecovery||@ohos.app.ability.appRecovery.d.ts|
|Deleted|Module name: ohos.app.ability.appRecovery<br>Class name: appReceovery<br>Method or attribute name: restartApp||@ohos.app.ability.appRecovery.d.ts|
|Deleted|Module name: ohos.app.ability.appRecovery<br>Class name: appReceovery<br>Method or attribute name: saveAppState||@ohos.app.ability.appRecovery.d.ts|
|Model changed|Class name: Ability<br>model: @stage model only|Class name: Ability<br>model: @Stage Model Only|@ohos.app.ability.Ability.d.ts|
|Model changed|Method or attribute name: onConfigurationUpdate<br>model: @stage model only|Method or attribute name: onConfigurationUpdate<br>model: @Stage Model Only|@ohos.app.ability.Ability.d.ts|
|Model changed|Method or attribute name: onMemoryLevel<br>model: @stage model only|Method or attribute name: onMemoryLevel<br>model: @Stage Model Only|@ohos.app.ability.Ability.d.ts|
|Model changed|Class name: AbilityConstant<br>model: @stage model only|Class name: AbilityConstant<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: LaunchParam<br>model: @stage model only|Class name: LaunchParam<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: launchReason<br>model: @stage model only|Method or attribute name: launchReason<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: lastExitReason<br>model: @stage model only|Method or attribute name: lastExitReason<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: LaunchReason<br>model: @stage model only|Class name: LaunchReason<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: UNKNOWN<br>model: @stage model only|Method or attribute name: UNKNOWN<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: START_ABILITY<br>model: @stage model only|Method or attribute name: START_ABILITY<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: CALL<br>model: @stage model only|Method or attribute name: CALL<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: CONTINUATION<br>model: @stage model only|Method or attribute name: CONTINUATION<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: APP_RECOVERY<br>model: @stage model only|Method or attribute name: APP_RECOVERY<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: LastExitReason<br>model: @stage model only|Class name: LastExitReason<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: UNKNOWN<br>model: @stage model only|Method or attribute name: UNKNOWN<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: ABILITY_NOT_RESPONDING<br>model: @stage model only|Method or attribute name: ABILITY_NOT_RESPONDING<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: NORMAL<br>model: @stage model only|Method or attribute name: NORMAL<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: OnContinueResult<br>model: @stage model only|Class name: OnContinueResult<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: AGREE<br>model: @stage model only|Method or attribute name: AGREE<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: REJECT<br>model: @stage model only|Method or attribute name: REJECT<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: MISMATCH<br>model: @stage model only|Method or attribute name: MISMATCH<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: MemoryLevel<br>model: @stage model only|Class name: MemoryLevel<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: MEMORY_LEVEL_MODERATE<br>model: @stage model only|Method or attribute name: MEMORY_LEVEL_MODERATE<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: MEMORY_LEVEL_LOW<br>model: @stage model only|Method or attribute name: MEMORY_LEVEL_LOW<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: MEMORY_LEVEL_CRITICAL<br>model: @stage model only|Method or attribute name: MEMORY_LEVEL_CRITICAL<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: WindowMode<br>model: @stage model only|Class name: WindowMode<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: WINDOW_MODE_UNDEFINED<br>model: @stage model only|Method or attribute name: WINDOW_MODE_UNDEFINED<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: WINDOW_MODE_FULLSCREEN<br>model: @stage model only|Method or attribute name: WINDOW_MODE_FULLSCREEN<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: WINDOW_MODE_SPLIT_PRIMARY<br>model: @stage model only|Method or attribute name: WINDOW_MODE_SPLIT_PRIMARY<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: WINDOW_MODE_SPLIT_SECONDARY<br>model: @stage model only|Method or attribute name: WINDOW_MODE_SPLIT_SECONDARY<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: WINDOW_MODE_FLOATING<br>model: @stage model only|Method or attribute name: WINDOW_MODE_FLOATING<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: OnSaveResult<br>model: @stage model only|Class name: OnSaveResult<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: ALL_AGREE<br>model: @stage model only|Method or attribute name: ALL_AGREE<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: CONTINUATION_REJECT<br>model: @stage model only|Method or attribute name: CONTINUATION_REJECT<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: CONTINUATION_MISMATCH<br>model: @stage model only|Method or attribute name: CONTINUATION_MISMATCH<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: RECOVERY_AGREE<br>model: @stage model only|Method or attribute name: RECOVERY_AGREE<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: RECOVERY_REJECT<br>model: @stage model only|Method or attribute name: RECOVERY_REJECT<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: ALL_REJECT<br>model: @stage model only|Method or attribute name: ALL_REJECT<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: StateType<br>model: @stage model only|Class name: StateType<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: CONTINUATION<br>model: @stage model only|Method or attribute name: CONTINUATION<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Method or attribute name: APP_RECOVERY<br>model: @stage model only|Method or attribute name: APP_RECOVERY<br>model: @Stage Model Only|@ohos.app.ability.AbilityConstant.d.ts|
|Model changed|Class name: AbilityLifecycleCallback<br>model: @stage model only|Class name: AbilityLifecycleCallback<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onAbilityCreate<br>model: @stage model only|Method or attribute name: onAbilityCreate<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onWindowStageCreate<br>model: @stage model only|Method or attribute name: onWindowStageCreate<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onWindowStageActive<br>model: @stage model only|Method or attribute name: onWindowStageActive<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onWindowStageInactive<br>model: @stage model only|Method or attribute name: onWindowStageInactive<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onWindowStageDestroy<br>model: @stage model only|Method or attribute name: onWindowStageDestroy<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onAbilityDestroy<br>model: @stage model only|Method or attribute name: onAbilityDestroy<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onAbilityForeground<br>model: @stage model only|Method or attribute name: onAbilityForeground<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onAbilityBackground<br>model: @stage model only|Method or attribute name: onAbilityBackground<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Method or attribute name: onAbilityContinue<br>model: @stage model only|Method or attribute name: onAbilityContinue<br>model: @Stage Model Only|@ohos.app.ability.AbilityLifecycleCallback.d.ts|
|Model changed|Class name: AbilityStage<br>model: @stage model only|Class name: AbilityStage<br>model: @Stage Model Only|@ohos.app.ability.AbilityStage.d.ts|
|Model changed|Method or attribute name: context<br>model: @stage model only|Method or attribute name: context<br>model: @Stage Model Only|@ohos.app.ability.AbilityStage.d.ts|
|Model changed|Method or attribute name: onCreate<br>model: @stage model only|Method or attribute name: onCreate<br>model: @Stage Model Only|@ohos.app.ability.AbilityStage.d.ts|
|Model changed|Method or attribute name: onAcceptWant<br>model: @stage model only|Method or attribute name: onAcceptWant<br>model: @Stage Model Only|@ohos.app.ability.AbilityStage.d.ts|
|Model changed|Method or attribute name: onConfigurationUpdate<br>model: @stage model only|Method or attribute name: onConfigurationUpdate<br>model: @Stage Model Only|@ohos.app.ability.AbilityStage.d.ts|
|Model changed|Method or attribute name: onMemoryLevel<br>model: @stage model only|Method or attribute name: onMemoryLevel<br>model: @Stage Model Only|@ohos.app.ability.AbilityStage.d.ts|
|Model changed|Class name: common<br>model: @stage model only|Class name: common<br>model: @Stage Model Only|@ohos.app.ability.common.d.ts|
|Model changed|Class name: AreaMode<br>model: @stage model only|Class name: AreaMode<br>model: @Stage Model Only|@ohos.app.ability.common.d.ts|
|Model changed|Method or attribute name: EL1<br>model: @stage model only|Method or attribute name: EL1<br>model: @Stage Model Only|@ohos.app.ability.common.d.ts|
|Model changed|Method or attribute name: EL2<br>model: @stage model only|Method or attribute name: EL2<br>model: @Stage Model Only|@ohos.app.ability.common.d.ts|
|Model changed|Method or attribute name: onConfigurationUpdated<br>model: @stage model only|Method or attribute name: onConfigurationUpdated<br>model: @Stage Model Only|@ohos.app.ability.EnvironmentCallback.d.ts|
|Model changed|Class name: ExtensionAbility<br>model: @stage model only|Class name: ExtensionAbility<br>model: @Stage Model Only|@ohos.app.ability.ExtensionAbility.d.ts|
|Model changed|Class name: ServiceExtensionAbility<br>model: @stage model only|Class name: ServiceExtensionAbility<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: context<br>model: @stage model only|Method or attribute name: context<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onCreate<br>model: @stage model only|Method or attribute name: onCreate<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onDestroy<br>model: @stage model only|Method or attribute name: onDestroy<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onRequest<br>model: @stage model only|Method or attribute name: onRequest<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onConnect<br>model: @stage model only|Method or attribute name: onConnect<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onDisconnect<br>model: @stage model only|Method or attribute name: onDisconnect<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onReconnect<br>model: @stage model only|Method or attribute name: onReconnect<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onConfigurationUpdate<br>model: @stage model only|Method or attribute name: onConfigurationUpdate<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Method or attribute name: onDump<br>model: @stage model only|Method or attribute name: onDump<br>model: @Stage Model Only|@ohos.app.ability.ServiceExtensionAbility.d.ts|
|Model changed|Class name: StartOptions<br>model: @stage model only|Class name: StartOptions<br>model: @Stage Model Only|@ohos.app.ability.StartOptions.d.ts|
|Model changed|Method or attribute name: windowMode<br>model: @stage model only|Method or attribute name: windowMode<br>model: @Stage Model Only|@ohos.app.ability.StartOptions.d.ts|
|Model changed|Method or attribute name: displayId<br>model: @stage model only|Method or attribute name: displayId<br>model: @Stage Model Only|@ohos.app.ability.StartOptions.d.ts|
|Model changed|Class name: OnReleaseCallback<br>model: @stage model only|Class name: OnReleaseCallback<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: OnReleaseCallback<br>model: @stage model only|Method or attribute name: OnReleaseCallback<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Class name: CalleeCallback<br>model: @stage model only|Class name: CalleeCallback<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: CalleeCallback<br>model: @stage model only|Method or attribute name: CalleeCallback<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Class name: Caller<br>model: @stage model only|Class name: Caller<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: call<br>model: @stage model only|Method or attribute name: call<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: callWithResult<br>model: @stage model only|Method or attribute name: callWithResult<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: release<br>model: @stage model only|Method or attribute name: release<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onRelease<br>model: @stage model only|Method or attribute name: onRelease<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: on_release<br>model: @stage model only|Method or attribute name: on_release<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: off_release<br>model: @stage model only|Method or attribute name: off_release<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: off_release<br>model: @stage model only|Method or attribute name: off_release<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Class name: Callee<br>model: @stage model only|Class name: Callee<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: on<br>model: @stage model only|Method or attribute name: on<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: off<br>model: @stage model only|Method or attribute name: off<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Class name: UIAbility<br>model: @stage model only|Class name: UIAbility<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: context<br>model: @stage model only|Method or attribute name: context<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: launchWant<br>model: @stage model only|Method or attribute name: launchWant<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: lastRequestWant<br>model: @stage model only|Method or attribute name: lastRequestWant<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: callee<br>model: @stage model only|Method or attribute name: callee<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onCreate<br>model: @stage model only|Method or attribute name: onCreate<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onWindowStageCreate<br>model: @stage model only|Method or attribute name: onWindowStageCreate<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onWindowStageDestroy<br>model: @stage model only|Method or attribute name: onWindowStageDestroy<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onWindowStageRestore<br>model: @stage model only|Method or attribute name: onWindowStageRestore<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onDestroy<br>model: @stage model only|Method or attribute name: onDestroy<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onForeground<br>model: @stage model only|Method or attribute name: onForeground<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onBackground<br>model: @stage model only|Method or attribute name: onBackground<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onContinue<br>model: @stage model only|Method or attribute name: onContinue<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onNewWant<br>model: @stage model only|Method or attribute name: onNewWant<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Method or attribute name: onDump<br>model: @stage model only|Method or attribute name: onDump<br>model: @Stage Model Only|@ohos.app.ability.UIAbility.d.ts|
|Model changed|Class name: FormExtensionAbility<br>model: @stage model only|Class name: FormExtensionAbility<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: context<br>model: @stage model only|Method or attribute name: context<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onAddForm<br>model: @stage model only|Method or attribute name: onAddForm<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onCastToNormalForm<br>model: @stage model only|Method or attribute name: onCastToNormalForm<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onUpdateForm<br>model: @stage model only|Method or attribute name: onUpdateForm<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onChangeFormVisibility<br>model: @stage model only|Method or attribute name: onChangeFormVisibility<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onFormEvent<br>model: @stage model only|Method or attribute name: onFormEvent<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onRemoveForm<br>model: @stage model only|Method or attribute name: onRemoveForm<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onConfigurationUpdate<br>model: @stage model only|Method or attribute name: onConfigurationUpdate<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onAcquireFormState<br>model: @stage model only|Method or attribute name: onAcquireFormState<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Method or attribute name: onShareForm<br>model: @stage model only|Method or attribute name: onShareForm<br>model: @Stage Model Only|@ohos.app.form.FormExtensionAbility.d.ts|
|Model changed|Class name: AbilityContext<br>model: @stage model only|Class name: AbilityContext<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: abilityInfo<br>model: @stage model only|Method or attribute name: abilityInfo<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: currentHapModuleInfo<br>model: @stage model only|Method or attribute name: currentHapModuleInfo<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: config<br>model: @stage model only|Method or attribute name: config<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityByCall<br>model: @stage model only|Method or attribute name: startAbilityByCall<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResult<br>model: @stage model only|Method or attribute name: startAbilityForResult<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResult<br>model: @stage model only|Method or attribute name: startAbilityForResult<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResult<br>model: @stage model only|Method or attribute name: startAbilityForResult<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResultWithAccount<br>model: @stage model only|Method or attribute name: startAbilityForResultWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResultWithAccount<br>model: @stage model only|Method or attribute name: startAbilityForResultWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResultWithAccount<br>model: @stage model only|Method or attribute name: startAbilityForResultWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbility<br>model: @stage model only|Method or attribute name: startServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbility<br>model: @stage model only|Method or attribute name: startServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbility<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbility<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelf<br>model: @stage model only|Method or attribute name: terminateSelf<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelf<br>model: @stage model only|Method or attribute name: terminateSelf<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelfWithResult<br>model: @stage model only|Method or attribute name: terminateSelfWithResult<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelfWithResult<br>model: @stage model only|Method or attribute name: terminateSelfWithResult<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: connectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: connectServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: connectServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: connectServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: disconnectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: disconnectServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: disconnectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: disconnectServiceExtensionAbility<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionLabel<br>model: @stage model only|Method or attribute name: setMissionLabel<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionLabel<br>model: @stage model only|Method or attribute name: setMissionLabel<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionIcon<br>model: @stage model only|Method or attribute name: setMissionIcon<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionIcon<br>model: @stage model only|Method or attribute name: setMissionIcon<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: requestPermissionsFromUser<br>model: @stage model only|Method or attribute name: requestPermissionsFromUser<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: requestPermissionsFromUser<br>model: @stage model only|Method or attribute name: requestPermissionsFromUser<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: restoreWindowStage<br>model: @stage model only|Method or attribute name: restoreWindowStage<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Method or attribute name: isTerminating<br>model: @stage model only|Method or attribute name: isTerminating<br>model: @Stage Model Only|AbilityContext.d.ts|
|Model changed|Class name: ApplicationContext<br>model: @stage model only|Class name: ApplicationContext<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: on_abilityLifecycle<br>model: @stage model only|Method or attribute name: on_abilityLifecycle<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: off_abilityLifecycle<br>model: @stage model only|Method or attribute name: off_abilityLifecycle<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: off_abilityLifecycle<br>model: @stage model only|Method or attribute name: off_abilityLifecycle<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: on_environment<br>model: @stage model only|Method or attribute name: on_environment<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: off_environment<br>model: @stage model only|Method or attribute name: off_environment<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: off_environment<br>model: @stage model only|Method or attribute name: off_environment<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: getProcessRunningInformation<br>model: @stage model only|Method or attribute name: getProcessRunningInformation<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: getProcessRunningInformation<br>model: @stage model only|Method or attribute name: getProcessRunningInformation<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: killProcessesBySelf<br>model: @stage model only|Method or attribute name: killProcessesBySelf<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Method or attribute name: killProcessesBySelf<br>model: @stage model only|Method or attribute name: killProcessesBySelf<br>model: @Stage Model Only|ApplicationContext.d.ts|
|Model changed|Class name: Context<br>model: @stage model only|Class name: Context<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: resourceManager<br>model: @stage model only|Method or attribute name: resourceManager<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: applicationInfo<br>model: @stage model only|Method or attribute name: applicationInfo<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: cacheDir<br>model: @stage model only|Method or attribute name: cacheDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: tempDir<br>model: @stage model only|Method or attribute name: tempDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: filesDir<br>model: @stage model only|Method or attribute name: filesDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: databaseDir<br>model: @stage model only|Method or attribute name: databaseDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: preferencesDir<br>model: @stage model only|Method or attribute name: preferencesDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: bundleCodeDir<br>model: @stage model only|Method or attribute name: bundleCodeDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: distributedFilesDir<br>model: @stage model only|Method or attribute name: distributedFilesDir<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: eventHub<br>model: @stage model only|Method or attribute name: eventHub<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: area<br>model: @stage model only|Method or attribute name: area<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: createBundleContext<br>model: @stage model only|Method or attribute name: createBundleContext<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: createModuleContext<br>model: @stage model only|Method or attribute name: createModuleContext<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: createModuleContext<br>model: @stage model only|Method or attribute name: createModuleContext<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: getApplicationContext<br>model: @stage model only|Method or attribute name: getApplicationContext<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Class name: AreaMode<br>model: @stage model only|Class name: AreaMode<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: EL1<br>model: @stage model only|Method or attribute name: EL1<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Method or attribute name: EL2<br>model: @stage model only|Method or attribute name: EL2<br>model: @Stage Model Only|Context.d.ts|
|Model changed|Class name: EventHub<br>model: @stage model only|Class name: EventHub<br>model: @Stage Model Only|EventHub.d.ts|
|Model changed|Method or attribute name: on<br>model: @stage model only|Method or attribute name: on<br>model: @Stage Model Only|EventHub.d.ts|
|Model changed|Method or attribute name: off<br>model: @stage model only|Method or attribute name: off<br>model: @Stage Model Only|EventHub.d.ts|
|Model changed|Method or attribute name: emit<br>model: @stage model only|Method or attribute name: emit<br>model: @Stage Model Only|EventHub.d.ts|
|Model changed|Class name: FormExtensionContext<br>model: @stage model only|Class name: FormExtensionContext<br>model: @Stage Model Only|FormExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|FormExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|FormExtensionContext.d.ts|
|Model changed|Class name: ServiceExtensionContext<br>model: @stage model only|Class name: ServiceExtensionContext<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbility<br>model: @stage model only|Method or attribute name: startServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbility<br>model: @stage model only|Method or attribute name: startServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbility<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbility<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: terminateSelf<br>model: @stage model only|Method or attribute name: terminateSelf<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: terminateSelf<br>model: @stage model only|Method or attribute name: terminateSelf<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: connectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: connectServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: connectServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: connectServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: disconnectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: disconnectServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: disconnectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: disconnectServiceExtensionAbility<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Method or attribute name: startAbilityByCall<br>model: @stage model only|Method or attribute name: startAbilityByCall<br>model: @Stage Model Only|ServiceExtensionContext.d.ts|
|Model changed|Class name: UIAbilityContext<br>model: @stage model only|Class name: UIAbilityContext<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: abilityInfo<br>model: @stage model only|Method or attribute name: abilityInfo<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: currentHapModuleInfo<br>model: @stage model only|Method or attribute name: currentHapModuleInfo<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: config<br>model: @stage model only|Method or attribute name: config<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbility<br>model: @stage model only|Method or attribute name: startAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityByCall<br>model: @stage model only|Method or attribute name: startAbilityByCall<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityWithAccount<br>model: @stage model only|Method or attribute name: startAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResult<br>model: @stage model only|Method or attribute name: startAbilityForResult<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResult<br>model: @stage model only|Method or attribute name: startAbilityForResult<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResult<br>model: @stage model only|Method or attribute name: startAbilityForResult<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResultWithAccount<br>model: @stage model only|Method or attribute name: startAbilityForResultWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResultWithAccount<br>model: @stage model only|Method or attribute name: startAbilityForResultWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startAbilityForResultWithAccount<br>model: @stage model only|Method or attribute name: startAbilityForResultWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbility<br>model: @stage model only|Method or attribute name: startServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbility<br>model: @stage model only|Method or attribute name: startServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: startServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbility<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbility<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: stopServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelf<br>model: @stage model only|Method or attribute name: terminateSelf<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelf<br>model: @stage model only|Method or attribute name: terminateSelf<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelfWithResult<br>model: @stage model only|Method or attribute name: terminateSelfWithResult<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: terminateSelfWithResult<br>model: @stage model only|Method or attribute name: terminateSelfWithResult<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: connectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: connectServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: connectServiceExtensionAbilityWithAccount<br>model: @stage model only|Method or attribute name: connectServiceExtensionAbilityWithAccount<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: disconnectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: disconnectServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: disconnectServiceExtensionAbility<br>model: @stage model only|Method or attribute name: disconnectServiceExtensionAbility<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionLabel<br>model: @stage model only|Method or attribute name: setMissionLabel<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionLabel<br>model: @stage model only|Method or attribute name: setMissionLabel<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionIcon<br>model: @stage model only|Method or attribute name: setMissionIcon<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: setMissionIcon<br>model: @stage model only|Method or attribute name: setMissionIcon<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: requestPermissionsFromUser<br>model: @stage model only|Method or attribute name: requestPermissionsFromUser<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: requestPermissionsFromUser<br>model: @stage model only|Method or attribute name: requestPermissionsFromUser<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: restoreWindowStage<br>model: @stage model only|Method or attribute name: restoreWindowStage<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Model changed|Method or attribute name: isTerminating<br>model: @stage model only|Method or attribute name: isTerminating<br>model: @Stage Model Only|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: restoreWindowStage<br>Access level: system API|Method or attribute name: restoreWindowStage<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: isTerminating<br>Access level: system API|Method or attribute name: isTerminating<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: restoreWindowStage<br>Access level: system API|Method or attribute name: restoreWindowStage<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: isTerminating<br>Access level: system API|Method or attribute name: isTerminating<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: restoreWindowStage<br>Access level: system API|Method or attribute name: restoreWindowStage<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: isTerminating<br>Access level: system API|Method or attribute name: isTerminating<br>Access level: public API|AbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: requestPermissionsFromUser<br>Access level: system API|Method or attribute name: requestPermissionsFromUser<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: restoreWindowStage<br>Access level: system API|Method or attribute name: restoreWindowStage<br>Access level: public API|UIAbilityContext.d.ts|
|Access level changed|Method or attribute name: isTerminating<br>Access level: system API|Method or attribute name: isTerminating<br>Access level: public API|UIAbilityContext.d.ts|
