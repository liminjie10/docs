| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.data.distributedData<br>Class name: KVStore<br>Method or attribute name: off_syncComplete|@ohos.data.distributedData.d.ts|
|Added||Module name: ohos.data.distributedData<br>Class name: SingleKVStore<br>Method or attribute name: on_dataChange|@ohos.data.distributedData.d.ts|
|Added||Module name: ohos.data.distributedData<br>Class name: SingleKVStore<br>Method or attribute name: off_dataChange|@ohos.data.distributedData.d.ts|
|Added||Module name: ohos.data.distributedData<br>Class name: DeviceKVStore<br>Method or attribute name: on_dataChange|@ohos.data.distributedData.d.ts|
|Added||Module name: ohos.data.distributedData<br>Class name: DeviceKVStore<br>Method or attribute name: off_dataChange|@ohos.data.distributedData.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: get|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: get|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getEntries|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getEntries|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getEntries|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getEntries|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSet|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSet|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSet|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSet|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSet|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSet|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSize|@ohos.data.distributedKVStore.d.ts|
|Added||Module name: ohos.data.distributedKVStore<br>Class name: DeviceKVStore<br>Method or attribute name: getResultSize|@ohos.data.distributedKVStore.d.ts|
|Access level changed|Method or attribute name: update<br>Access level: public API|Method or attribute name: update<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: update<br>Access level: public API|Method or attribute name: update<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: delete<br>Access level: public API|Method or attribute name: delete<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: delete<br>Access level: public API|Method or attribute name: delete<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: query<br>Access level: public API|Method or attribute name: query<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: query<br>Access level: public API|Method or attribute name: query<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: update<br>Access level: public API|Method or attribute name: update<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: update<br>Access level: public API|Method or attribute name: update<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: delete<br>Access level: public API|Method or attribute name: delete<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: delete<br>Access level: public API|Method or attribute name: delete<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: query<br>Access level: public API|Method or attribute name: query<br>Access level: system API|@ohos.data.rdb.d.ts|
|Access level changed|Method or attribute name: query<br>Access level: public API|Method or attribute name: query<br>Access level: system API|@ohos.data.rdb.d.ts|
