| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.stationary<br>Class name: stationary|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityResponse|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityResponse<br>Method or attribute name: state|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityEvent|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityEvent<br>Method or attribute name: ENTER|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityEvent<br>Method or attribute name: EXIT|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityEvent<br>Method or attribute name: ENTER_EXIT|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityState|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityState<br>Method or attribute name: ENTER|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: ActivityState<br>Method or attribute name: EXIT|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: stationary<br>Method or attribute name: on|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: stationary<br>Method or attribute name: once|@ohos.stationary.d.ts|
|Added||Module name: ohos.stationary<br>Class name: stationary<br>Method or attribute name: off|@ohos.stationary.d.ts|
