| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: bundleName<br>Function name: bundleName?: string;|@ohos.uitest.d.ts|
|Added||Method or attribute name: title<br>Function name: title?: string;|@ohos.uitest.d.ts|
|Added||Method or attribute name: focused<br>Function name: focused?: boolean;|@ohos.uitest.d.ts|
|Added||Method or attribute name: actived<br>Function name: actived?: boolean;|@ohos.uitest.d.ts|
