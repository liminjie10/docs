| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Deprecated version changed|Class name: bytrace<br>Deprecated version: N/A|Class name: bytrace<br>Deprecated version: 8<br>Deprecated version: ohos.hiTraceMeter  |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: startTrace<br>Deprecated version: N/A|Method or attribute name: startTrace<br>Deprecated version: 8<br>Deprecated version: ohos.hiTraceMeter.startTrace   |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: finishTrace<br>Deprecated version: N/A|Method or attribute name: finishTrace<br>Deprecated version: 8<br>Deprecated version: ohos.hiTraceMeter.finishTrace   |@ohos.bytrace.d.ts|
|Deprecated version changed|Method or attribute name: traceByValue<br>Deprecated version: N/A|Method or attribute name: traceByValue<br>Deprecated version: 8<br>Deprecated version: ohos.hiTraceMeter.traceByValue   |@ohos.bytrace.d.ts|
