| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: bundleName<br>Function name: readonly bundleName: string;|@ohos.bundle.bundleMonitor.d.ts|
|Added||Method or attribute name: userId<br>Function name: readonly userId: number;|@ohos.bundle.bundleMonitor.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: defaultAppManager|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: BROWSER|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: IMAGE|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: AUDIO|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: VIDEO|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: PDF|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: WORD|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: EXCEL|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: ohos.bundle.defaultAppManager<br>Class name: ApplicationType<br>Method or attribute name: PPT|@ohos.bundle.defaultAppManager.d.ts|
|Added||Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: metadata|abilityInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: metadata|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: iconResource|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: labelResource|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: descriptionResource|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: appDistributionType|applicationInfo.d.ts|
|Added||Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: appProvisionType|applicationInfo.d.ts|
|Added||Module name: bundleInfo<br>Class name: ReqPermissionDetail<br>Method or attribute name: reasonId|bundleInfo.d.ts|
|Added||Module name: dispatchInfo<br>Class name: DispatchInfo|dispatchInfo.d.ts|
|Added||Module name: dispatchInfo<br>Class name: DispatchInfo<br>Method or attribute name: version|dispatchInfo.d.ts|
|Added||Module name: elementName<br>Class name: ElementName<br>Method or attribute name: moduleName|elementName.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: bundleName|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: moduleName|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: name|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: labelId|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: descriptionId|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: iconId|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: isVisible|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: permissions|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: applicationInfo|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: metadata|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: enabled|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: readPermission|extensionAbilityInfo.d.ts|
|Added||Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: writePermission|extensionAbilityInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: mainElementName|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: metadata|hapModuleInfo.d.ts|
|Added||Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: hashValue|hapModuleInfo.d.ts|
|Added||Module name: metadata<br>Class name: Metadata<br>Method or attribute name: name|metadata.d.ts|
|Added||Module name: metadata<br>Class name: Metadata<br>Method or attribute name: value|metadata.d.ts|
|Added||Module name: metadata<br>Class name: Metadata<br>Method or attribute name: resource|metadata.d.ts|
|Added||Module name: packInfo<br>Class name: BundlePackInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundlePackInfo<br>Method or attribute name: packages|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundlePackInfo<br>Method or attribute name: summary|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: moduleType|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: deliveryWithInstall|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageSummary|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageSummary<br>Method or attribute name: app|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: PackageSummary<br>Method or attribute name: modules|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundleConfigInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundleConfigInfo<br>Method or attribute name: bundleName|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: BundleConfigInfo<br>Method or attribute name: version|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: apiVersion|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: distro|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: abilities|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: deliveryWithInstall|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: installationFree|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: moduleName|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: moduleType|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: label|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: visible|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ModuleAbilityInfo<br>Method or attribute name: forms|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: type|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: updateEnabled|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: scheduledUpdateTime|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: updateDuration|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version<br>Method or attribute name: minCompatibleVersionCode|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version<br>Method or attribute name: name|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: Version<br>Method or attribute name: code|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion<br>Method or attribute name: releaseType|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion<br>Method or attribute name: compatible|packInfo.d.ts|
|Added||Module name: packInfo<br>Class name: ApiVersion<br>Method or attribute name: target|packInfo.d.ts|
|Added||Method or attribute name: permissionName<br>Function name: readonly permissionName: string;|permissionDef.d.ts|
|Added||Method or attribute name: grantMode<br>Function name: readonly grantMode: number;|permissionDef.d.ts|
|Added||Method or attribute name: labelId<br>Function name: readonly labelId: number;|permissionDef.d.ts|
|Added||Method or attribute name: descriptionId<br>Function name: readonly descriptionId: number;|permissionDef.d.ts|
|Added||Module name: shortcutInfo<br>Class name: ShortcutWant<br>Method or attribute name: targetModule|shortcutInfo.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_WITH_EXTENSION_ABILITY||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: BundleFlag<br>Method or attribute name: GET_BUNDLE_WITH_HASH_VALUE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: BundleFlag<br>Method or attribute name: GET_APPLICATION_INFO_WITH_CERTIFICATE_FINGERPRINT||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionFlag||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_DEFAULT||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_WITH_PERMISSION||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_WITH_APPLICATION||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionFlag<br>Method or attribute name: GET_EXTENSION_INFO_WITH_METADATA||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: LANDSCAPE_INVERTED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: PORTRAIT_INVERTED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_LANDSCAPE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_PORTRAIT||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_RESTRICTED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_LANDSCAPE_RESTRICTED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: AUTO_ROTATION_PORTRAIT_RESTRICTED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: DisplayOrientation<br>Method or attribute name: LOCKED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: FORM||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: WORK_SCHEDULER||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: INPUT_METHOD||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: SERVICE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: ACCESSIBILITY||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: DATA_SHARE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: FILE_SHARE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: STATIC_SUBSCRIBER||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: WALLPAPER||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: BACKUP||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: WINDOW||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: ENTERPRISE_ADMIN||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: THUMBNAIL||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: PREVIEW||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: ExtensionAbilityType<br>Method or attribute name: UNSPECIFIED||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: UpgradeFlag||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: UpgradeFlag<br>Method or attribute name: NOT_UPGRADE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: UpgradeFlag<br>Method or attribute name: SINGLE_UPGRADE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: UpgradeFlag<br>Method or attribute name: RELATION_UPGRADE||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: SupportWindowMode||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: SupportWindowMode<br>Method or attribute name: FULL_SCREEN||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: SupportWindowMode<br>Method or attribute name: SPLIT||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: SupportWindowMode<br>Method or attribute name: FLOATING||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: queryExtensionAbilityInfos||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: queryExtensionAbilityInfos||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: queryExtensionAbilityInfos||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setModuleUpgradeFlag||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setModuleUpgradeFlag||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: isModuleRemovable||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: isModuleRemovable||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundlePackInfo||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundlePackInfo||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getAbilityInfo||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getAbilityInfo||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDispatcherVersion||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDispatcherVersion||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getAbilityLabel||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getAbilityLabel||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getAbilityIcon||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getAbilityIcon||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByAbility||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByAbility||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByExtensionAbility||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getProfileByExtensionAbility||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setDisposedStatus||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: setDisposedStatus||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDisposedStatus||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getDisposedStatus||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getApplicationInfoSync||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getApplicationInfoSync||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundleInfoSync||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.bundle<br>Class name: bundle<br>Method or attribute name: getBundleInfoSync||@ohos.bundle.d.ts|
|Deleted|Module name: ohos.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo||@ohos.distributedBundle.d.ts|
|Deleted|Module name: ohos.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfo||@ohos.distributedBundle.d.ts|
|Deleted|Module name: ohos.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfos||@ohos.distributedBundle.d.ts|
|Deleted|Module name: ohos.distributedBundle<br>Class name: distributedBundle<br>Method or attribute name: getRemoteAbilityInfos||@ohos.distributedBundle.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: supportWindowMode||abilityInfo.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: maxWindowRatio||abilityInfo.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: minWindowRatio||abilityInfo.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: maxWindowWidth||abilityInfo.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: minWindowWidth||abilityInfo.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: maxWindowHeight||abilityInfo.d.ts|
|Deleted|Module name: abilityInfo<br>Class name: AbilityInfo<br>Method or attribute name: minWindowHeight||abilityInfo.d.ts|
|Deleted|Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: labelIndex||applicationInfo.d.ts|
|Deleted|Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: iconIndex||applicationInfo.d.ts|
|Deleted|Module name: applicationInfo<br>Class name: ApplicationInfo<br>Method or attribute name: fingerprint||applicationInfo.d.ts|
|Deleted|Module name: bundleInfo<br>Class name: BundleInfo<br>Method or attribute name: extensionAbilityInfo||bundleInfo.d.ts|
|Deleted|Module name: bundleInstaller<br>Class name: HashParam||bundleInstaller.d.ts|
|Deleted|Module name: bundleInstaller<br>Class name: HashParam<br>Method or attribute name: moduleName||bundleInstaller.d.ts|
|Deleted|Module name: bundleInstaller<br>Class name: HashParam<br>Method or attribute name: hashValue||bundleInstaller.d.ts|
|Deleted|Module name: bundleInstaller<br>Class name: InstallParam<br>Method or attribute name: hashParams||bundleInstaller.d.ts|
|Deleted|Module name: bundleInstaller<br>Class name: InstallParam<br>Method or attribute name: crowdtestDeadline||bundleInstaller.d.ts|
|Deleted|Module name: dispatchInfo<br>Class name: DispatchInfo<br>Method or attribute name: dispatchAPI||dispatchInfo.d.ts|
|Deleted|Module name: extensionAbilityInfo<br>Class name: ExtensionAbilityInfo<br>Method or attribute name: extensionAbilityType||extensionAbilityInfo.d.ts|
|Deleted|Module name: hapModuleInfo<br>Class name: HapModuleInfo<br>Method or attribute name: extensionAbilityInfo||hapModuleInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: PackageConfig<br>Method or attribute name: deviceType||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: ExtensionAbilities||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: ExtensionAbilities<br>Method or attribute name: name||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: ExtensionAbilities<br>Method or attribute name: forms||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: deviceType||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: ModuleConfigInfo<br>Method or attribute name: extensionAbilities||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: ModuleDistroInfo<br>Method or attribute name: mainAbility||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: supportDimensions||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: AbilityFormInfo<br>Method or attribute name: defaultDimension||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: BundlePackFlag||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_PACK_INFO_ALL||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_PACKAGES||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_BUNDLE_SUMMARY||packInfo.d.ts|
|Deleted|Module name: packInfo<br>Class name: BundlePackFlag<br>Method or attribute name: GET_MODULE_SUMMARY||packInfo.d.ts|
|Deleted|Module name: shortcutInfo<br>Class name: ShortcutInfo<br>Method or attribute name: moduleName||shortcutInfo.d.ts|
|Deprecated version changed|Class name: innerBundleManager<br>Deprecated version: N/A|Class name: innerBundleManager<br>Deprecated version: 9<br>New API: ohos.bundle.launcherBundleManager |@ohos.bundle.innerBundleManager.d.ts|
|Deprecated version changed|Class name: CheckPackageHasInstalledResponse<br>Deprecated version: N/A|Class name: CheckPackageHasInstalledResponse<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Class name: CheckPackageHasInstalledOptions<br>Deprecated version: N/A|Class name: CheckPackageHasInstalledOptions<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Class name: Package<br>Deprecated version: N/A|Class name: Package<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Method or attribute name: hasInstalled<br>Deprecated version: N/A|Method or attribute name: hasInstalled<br>Deprecated version: 9|@system.package.d.ts|
|Deprecated version changed|Class name: ExtensionAbilityInfo<br>Deprecated version: 9|Class name: ExtensionAbilityInfo<br>Deprecated version: N/A|extensionAbilityInfo.d.ts|
|Deprecated version changed|Class name: Metadata<br>Deprecated version: 9|Class name: Metadata<br>Deprecated version: N/A|metadata.d.ts|
