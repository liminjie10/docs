| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: setProperty<br>Function name: setProperty(request: SetPropertyRequest, callback: AsyncCallback<void>): void;|@ohos.account.osAccount.d.ts|
|Added||Method or attribute name: setProperty<br>Function name: setProperty(request: SetPropertyRequest): Promise<void>;|@ohos.account.osAccount.d.ts|
