| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: backgroundTaskManager|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: DelaySuspendInfo|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: DelaySuspendInfo<br>Method or attribute name: requestId|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: DelaySuspendInfo<br>Method or attribute name: actualDelayTime|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: backgroundTaskManager<br>Method or attribute name: cancelSuspendDelay|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: backgroundTaskManager<br>Method or attribute name: requestSuspendDelay|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: backgroundTaskManager<br>Method or attribute name: applyEfficiencyResources|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: backgroundTaskManager<br>Method or attribute name: resetAllEfficiencyResources|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: DATA_TRANSFER|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: AUDIO_PLAYBACK|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: AUDIO_RECORDING|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: LOCATION|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: BLUETOOTH_INTERACTION|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: MULTI_DEVICE_CONNECTION|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: WIFI_INTERACTION|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: VOIP|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: BackgroundMode<br>Method or attribute name: TASK_KEEPING|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: CPU|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: COMMON_EVENT|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: TIMER|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: WORK_SCHEDULER|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: BLUETOOTH|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: GPS|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: ResourceType<br>Method or attribute name: AUDIO|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest<br>Method or attribute name: resourceTypes|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest<br>Method or attribute name: isApply|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest<br>Method or attribute name: timeOut|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest<br>Method or attribute name: isPersist|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest<br>Method or attribute name: isProcess|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Added||Module name: ohos.resourceschedule.backgroundTaskManager<br>Class name: EfficiencyResourcesRequest<br>Method or attribute name: reason|@ohos.resourceschedule.backgroundTaskManager.d.ts|
|Deprecated version changed|Method or attribute name: startBackgroundRunning<br>Deprecated version: N/A|Method or attribute name: startBackgroundRunning<br>Deprecated version: 9<br>New API: ohos.resourceschedule.backgroundTaskManager.startBackgroundRunning   |@ohos.ability.particleAbility.d.ts|
|Deprecated version changed|Method or attribute name: startBackgroundRunning<br>Deprecated version: N/A|Method or attribute name: startBackgroundRunning<br>Deprecated version: 9|@ohos.ability.particleAbility.d.ts|
|Deprecated version changed|Method or attribute name: cancelBackgroundRunning<br>Deprecated version: N/A|Method or attribute name: cancelBackgroundRunning<br>Deprecated version: 9<br>New API: ohos.resourceschedule.backgroundTaskManager.stopBackgroundRunning   |@ohos.ability.particleAbility.d.ts|
|Deprecated version changed|Method or attribute name: cancelBackgroundRunning<br>Deprecated version: N/A|Method or attribute name: cancelBackgroundRunning<br>Deprecated version: 9|@ohos.ability.particleAbility.d.ts|
