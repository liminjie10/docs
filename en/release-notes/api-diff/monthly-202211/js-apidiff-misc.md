| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Module name: ohos.inputmethodengine<br>Class name: InputMethodAbility<br>Method or attribute name: off_setSubtype|@ohos.inputmethodengine.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_PERMISSION||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_PARAMCHECK||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_UNSUPPORTED||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_PACKAGEMANAGER||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_IMENGINE||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_IMCLIENT||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_KEYEVENT||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_CONFPERSIST||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_CONTROLLER||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_SETTINGS||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_IMMS||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethod<br>Class name: inputMethod<br>Method or attribute name: EXCEPTION_OTHERS||@ohos.inputmethod.d.ts|
|Deleted|Module name: ohos.inputmethodengine<br>Class name: InputMethodAbility<br>Method or attribute name: off||@ohos.inputmethodengine.d.ts|
