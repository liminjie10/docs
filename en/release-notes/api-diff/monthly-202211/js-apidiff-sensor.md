| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: minSamplePeriod<br>Function name: minSamplePeriod:number;|@ohos.sensor.d.ts|
|Added||Method or attribute name: maxSamplePeriod<br>Function name: maxSamplePeriod:number;|@ohos.sensor.d.ts|
