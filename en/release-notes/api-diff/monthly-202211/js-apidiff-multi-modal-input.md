| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: supportKeys<br>Function name: function supportKeys(deviceId: number, keys: Array<KeyCode>, callback: AsyncCallback<Array<boolean>>): void;|@ohos.multimodalInput.inputDevice.d.ts|
