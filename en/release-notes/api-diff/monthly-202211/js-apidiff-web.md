| Change Type | New Version | Old Version | d.ts File |
| ---- | ------ | ------ | -------- |
|Added||Method or attribute name: saveCookieAsync<br>Function name: static saveCookieAsync(): Promise<void>;|@ohos.web.webview.d.ts|
|Added||Method or attribute name: saveCookieAsync<br>Function name: static saveCookieAsync(callback: AsyncCallback<void>): void;|@ohos.web.webview.d.ts|
|Added||Method or attribute name: stop<br>Function name: stop(): void;|@ohos.web.webview.d.ts|
|Deleted|Module name: ohos.web.webview<br>Class name: WebCookieManager<br>Method or attribute name: saveCookieSync||@ohos.web.webview.d.ts|
